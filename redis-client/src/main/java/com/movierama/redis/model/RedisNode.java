package com.movierama.redis.model;

import java.util.Map;
import java.util.Set;

/**
 * @author cskopelitis
 * @since 1.0.0
 */
public class RedisNode {

    private String host;
    private int port;

    private Set<RedisNode> masterOf;
    private RedisNode slaveOf;

    private String usedMemory;

    private Map<String, String> persistence;

    RedisNode() {
    }

    public RedisNode(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getNodeUri() {
        return String.format("%s:%s", host, port);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RedisNode that = (RedisNode) o;

        return port == that.port && !(host != null ? !host.equals(that.host) : that.host != null);
    }

    @Override
    public int hashCode() {
        int result = host != null ? host.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }

    public boolean isMaster() {
        return slaveOf == null;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public Set<RedisNode> getMasterOf() {
        return masterOf;
    }

    public void setMasterOf(Set<RedisNode> masterOf) {
        this.masterOf = masterOf;
    }

    public RedisNode getSlaveOf() {
        return slaveOf;
    }

    public void setSlaveOf(RedisNode slaveOf) {
        if (!"null".equals(slaveOf.getHost())) {
            this.slaveOf = slaveOf;
        }
    }

    public String getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(String usedMemory) {
        this.usedMemory = usedMemory;
    }

    public Map<String, String> getPersistence() {
        return persistence;
    }

    public void setPersistence(Map<String, String> persistence) {
        this.persistence = persistence;
    }
}
