package com.movierama.redis.model;

import java.util.Set;

/**
 * @author cskopelitis
 * @since 3.0
 */
public class RedisClusterState {

    private RedisNode master;
    private Set<RedisNode> slaves;

    private String clusterStatus;

    private Set<RedisNode> infoNodeID;

    public RedisNode getMaster() {
        return master;
    }

    public void setMaster(RedisNode master) {
        this.master = master;
    }

    public Set<RedisNode> getSlaves() {
        return slaves;
    }

    public void setSlaves(Set<RedisNode> slaves) {
        this.slaves = slaves;
    }

    public String getClusterStatus() {
        return clusterStatus;
    }

    public void setClusterStatus(String clusterStatus) {
        this.clusterStatus = clusterStatus;
    }

    public Set<RedisNode> getInfoNodeID() {
        return infoNodeID;
    }

    public void setInfoNodeID(Set<RedisNode> infoNodeID) {
        this.infoNodeID = infoNodeID;
    }
}
