package com.movierama.redis.types;

import java.util.Map;
import java.util.Set;

/**
 * @author cskopelitis
 * @since 2.0
 */
public interface RedisOrderedHash {

    Map<String, String> getFields(String hashId, String hashIndexId, Long min, Long max);

    void incrBy(String hashId, String hashIndexId, String field, Double score, Long value);

    void batchIncrBy(String hashId, String hashIndexId, Map<String, Double> fieldNamesWithScores, Long value);

    void batchDelete(String hashId, String hashIndexId, Set<String> fieldNames, Long min, Long max);
}
