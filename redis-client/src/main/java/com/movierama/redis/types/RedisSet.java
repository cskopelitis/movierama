package com.movierama.redis.types;

import java.util.Set;

/**
 * A wrapper for Redis Set native object
 *
 * @author cskopelitis
 * @see <a href="http://redis.io/commands#set">Redis Set</a>
 * @since 1.2.0
 */
public interface RedisSet {

    String pop(String setId);

    /**
     * Adds a new item to the set
     *
     * @param setId  the unique key of the set
     * @param member the member
     */
    void add(String setId, String member);

    /**
     * Batch add a list of members
     *
     * @param setId   the unique key of the set
     * @param members the list of the members to batch add
     */
    void addAll(String setId, Set<String> members);

    /**
     * Return all members in a set
     *
     * @param setId the id of the set
     *
     * @return a set containing all members of the set
     */
    Set<String> members(String setId);

    /**
     * Checks if the provided member is contained in the set
     *
     * @param setId  the unique key of the set
     * @param member the member that is validated
     *
     * @return true if member is contained in the set
     */
    boolean in(String setId, String member);

    /**
     * @param setId the unique key of the set
     *
     * @return Returns the number of items in the set
     */
    long length(String setId);
}
