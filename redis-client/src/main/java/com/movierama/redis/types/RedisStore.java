package com.movierama.redis.types;

import java.util.Set;

/**
 * Wrapper for the Redis String store
 *
 * @author cskopelitis
 * @since 1.2.0
 */
public interface RedisStore {

    /**
     * @param key the key
     *
     * @return the <code>java.lang.String</code> value
     * @see <a href="http://redis.io/commands/get"></a>
     */
    String get(String key);

    /**
     * @param key   the key
     * @param value the <code>java.lang.String</code> value
     *
     * @see <a href="http://redis.io/commands/set"></a>
     */
    void set(String key, String value);

    /**
     * @param pattern the pattern
     *
     * @return a list of the keys that matched the provided pattern
     * @see <a href="http://redis.io/commands/keys"></a>
     */
    Set<String> keys(String pattern);

    /**
     * Removes the given key from the store, along with all data
     *
     * @param key the key to remove
     */
    void delete(String key);

    /**
     * Determines whether this key exists in the store
     *
     * @param key the key to look for
     *
     * @see <a href="http://redis.io/commands/keys"></a>
     */
    boolean exists(String key);

    /**
     * Set Time To Live
     *
     * @param key     the key
     * @param seconds the time in seconds the key has to live
     */
    void setTtl(String key, int seconds);
}
