package com.movierama.redis.types;

import java.util.Map;
import java.util.Set;

/**
 * Wrapper for Redis' Hash data-type
 *
 * @author cskopelitis
 * @see <a href="http://redis.io/commands#hash">Redis Hashes</a>
 * @since 1.2.0
 */
public interface RedisHash {

    /**
     * Get the value from the given fieldName, from hash with the given hashId
     *
     * @param hashId    the unique hash id
     * @param fieldName the key
     *
     * @return the string/JSON value of the provided field
     */
    String get(String hashId, String fieldName);

    /**
     * Sets the value of the provided hash key
     *
     * @param hashId    the unique hash id
     * @param fieldName the key
     * @param value     the string/JSON value
     * @param overwrite if false, the value is left unchanged
     *
     * @throws com.velti.lsc.data.nosql.redis.exception.RedisStoreException in case of an error
     */
    void set(String hashId, String fieldName, String value, boolean overwrite);

    /**
     * Sets multiple fields with the provided values in the same hash
     *
     * @param hashId           the unique hash id
     * @param fieldsWithValues a list of key/value pairs
     * @param overwrite        if false, the values are left unchanged
     */
    void batchSet(String hashId, Map<String, String> fieldsWithValues, boolean overwrite);

    /**
     * Sets the values of the same field in multiple hashes
     *
     * @param hashIdsWithValue a list of unique hash ids with value pairs
     * @param fieldName        the key
     */
    void batchSet(Map<String, String> hashIdsWithValue, String fieldName);

    /**
     * Deletes the provided key from the hash
     *
     * @param hashId    the unique hash id
     * @param fieldName the key
     */
    void delete(String hashId, String fieldName);

    /**
     * @param hashId the unique hash id
     *
     * @return a list of key/value pairs of all the keys of the hash
     */
    Map<String, String> getAll(String hashId);

    /**
     * Increases the value of a hash field by the given amount
     *
     * @param hashId    the unique hash id
     * @param fieldName the hash field name
     * @param value     the amount to increase the value with
     */
    void incrBy(String hashId, String fieldName, Long value);

    /**
     * Increases the value of a multiple keys by the given amount
     *
     * @param hashId     the unique hash id
     * @param fieldNames the hash field name
     * @param value      the amount to increase the value with
     */
    void batchIncrBy(String hashId, Set<String> fieldNames, Long value);

    /**
     * Increases the value of the same key in multiple hashes by the given amount
     *
     * @param hashIds   a list of the affected hash ids
     * @param fieldName the key
     * @param value     the amount to increase the value with
     */
    void batchIncrBy(Set<String> hashIds, String fieldName, Long value);

    /**
     * Retrieves the size of the hash
     *
     * @param hashId the id of the hash
     *
     * @return the number of keys of the hash
     */
    long size(String hashId);
}
