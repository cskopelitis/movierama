package com.movierama.redis.types;

import redis.clients.jedis.Tuple;

import java.util.Map;
import java.util.Set;

/**
 * A wrapper for Redis Ordered Set native object
 *
 * @author cskopelitis
 * @see <a href="http://redis.io/commands#sorted_set">Redis Sorted Sets</a>
 * @since 1.2.0
 */
public interface RedisOrderedSet {

    /**
     * Single add of member with its score
     *
     * @param setId  the id of the set
     * @param member the member
     * @param score  the score
     */
    void add(String setId, String member, Double score);

    /**
     * Batch add of members + scores
     *
     * @param setId            the unique key of the ordered set
     * @param membersWithScore a member/score list
     */
    void batchAdd(String setId, Map<String, Double> membersWithScore);

    /**
     * Increases the score of a member by the given amount
     *
     * @param setId  the unique key of the ordered set
     * @param amount the amount to increment (if negative then decrement)
     * @param member the member
     */
    void incrScore(String setId, Long amount, String member);

    Set<Tuple> rangeWithScores(String setId, long min, long max);

    /**
     * Retrieve the score of the provided member
     *
     * @param setId  the unique key of the ordered set
     * @param member the member
     *
     * @return the score of the member, or ???? if it is not found
     */
    Double score(String setId, String member);

    /**
     * Retrieves the values of the specified range of scores
     *
     * @param setId the unique key of the ordered set
     * @param min   the MIN score
     * @param max   the MAX score
     * @param asc   true if ascending
     *
     * @return a list of the members
     * @throws java.util.NoSuchElementException when the setId does not exist
     */
    Set<String> range(String setId, Double min, Double max, boolean asc);

    /**
     * Removes the members of the specified range of scores
     *
     * @param setId the unique key of the ordered set
     * @param min   the MIN score to remove
     * @param max   the MAX score to remove
     *
     * @return the number of items removed
     */
    Long rangeRemove(String setId, Double min, Double max);

    /**
     * Removes the provided set of keys from the set. Use this method carefully,
     * since it is expensive. Alternatively, use <code>rangeRemove(String, Double, Double)</code>
     *
     * @param setId the unique key of the ordered set
     * @param keys  a list of the keys to remove
     *
     * @see RedisOrderedSet#rangeRemove(String, Double, Double)
     */
    void rangeRemove(String setId, Set<String> keys);

    /**
     * Implementation of the ZRANK which Returns the rank of member in the sorted set stored at key, with the scores
     * ordered from low to high. The rank (or index) is 0-based, which means that the member with the lowest score
     * has rank 0.
     *
     * @param setId  the unique key of the ordered set
     * @param member the member
     *
     * @return the rank of the member in the sorted set
     */
    Long rank(String setId, String member);
}
