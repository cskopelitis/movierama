package com.movierama.redis;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.BaseObjectPoolConfig;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.BinaryJedis;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.util.Pool;

import java.util.Iterator;
import java.util.List;

/** @see <a href="https://gist.github.com/1084272">source</a> */
public final class RedisRoundRobinPool extends Pool<Jedis> {

    private int poolSize;

    public RedisRoundRobinPool(final GenericObjectPoolConfig poolConfig, List<JedisShardInfo> shards) {
        initPool(poolConfig, new RoundRobinFactory(shards));
        poolSize = shards.size();
    }

    public RedisRoundRobinPool(final GenericObjectPoolConfig poolConfig, PooledObjectFactory<Jedis> factory) {
        super(poolConfig, factory);
    }

    @Override
    public void initPool(final GenericObjectPoolConfig poolConfig, PooledObjectFactory<Jedis> factory) {
        super.initPool(poolConfig, factory);
        internalPool.setLifo(false);
        internalPool.setMaxWaitMillis(100);
        addObjects(poolSize);
    }

    /*
    public RedisRoundRobinPool chainGetResource(Jedis result) {
        try {
            result = (Jedis) internalPool.borrowObject();
        } catch (Exception e) {
            throw new JedisConnectionException("Could not get a resource from the pool", e);
        }
        return this;
    }
	*/

    /**
     * Specifies the behavior of the borrowObject() method when the pool is exhausted:
     * <ul>
     * <li>When whenExhaustedAction is WHEN_EXHAUSTED_FAIL, borrowObject() will throw a NoSuchElementException</li>
     * <li>When whenExhaustedAction is WHEN_EXHAUSTED_GROW, borrowObject() will create a new object and return it
     * (essentially making maxActive meaningless.)</li>
     * <li>When whenExhaustedAction is WHEN_EXHAUSTED_BLOCK, borrowObject() will block (invoke Object.wait()) until a
     * new or idle object is available. If a positive maxWait value is supplied, then borrowObject() will block for at
     * most that many milliseconds, after which a NoSuchElementException will be thrown. If maxWait is non-positive,
     * the borrowObject() method will block indefinitely.</li>
     * </ul>
     */
    public void setBlockWhenExhausted(boolean whenExhaustedGrow) {
        internalPool.setBlockWhenExhausted(BaseObjectPoolConfig.DEFAULT_BLOCK_WHEN_EXHAUSTED);
    }

    public void setMinIdle(int minIdle) {
        internalPool.setMinIdle(minIdle);
    }

    public void setMaxIdle(int maxIdle) {
        internalPool.setMaxIdle(maxIdle);
    }

    /** @param stob When testOnBorrow is set, the pool will attempt to validate each object before it */
    public void setTestOnBorrow(boolean stob) {
        internalPool.setTestOnBorrow(stob);
    }

    /** @param stor When testOnReturn is set, the pool will attempt to validate each object before it is returned */
    public void setTestOnReturn(boolean stor) {
        internalPool.setTestOnReturn(stor);
    }

    class RoundRobinFactory extends BasePooledObjectFactory<Jedis> {

        private List<JedisShardInfo> shards;
        private Iterator<JedisShardInfo> shardIterator;

        void addSlave(JedisShardInfo jsi) {
            shards.add(jsi);
            shardIterator = shards.iterator();
        }

        RoundRobinFactory(List<JedisShardInfo> shards) {
            this.shards = shards;
            this.shardIterator = this.shards.iterator();
        }

        /*
        @Override
        public Object makeObject() {
            JedisShardInfo jsi = null;
            if (shardIterator.hasNext()) { jsi = shardIterator.next(); } else {
                shardIterator = this.shards.iterator();
                if (shardIterator.hasNext()) {
                    jsi = shardIterator.next();
                }
            }

            if (logger.isTraceEnabled()) {
                logger.trace(String.format("Returning %s:%s from pool", jsi.getHost(), jsi.getPort()));
            }

            return new Jedis(jsi.getHost(), jsi.getPort());
        }

        @Override
        public void destroyObject(final Object obj) {
            if ((obj != null) && (obj instanceof Jedis)) {
                Jedis jedis = (Jedis) obj;
                try {
                    try {
                        jedis.quit();
                    } catch (Exception ignored) {
                    }
                    jedis.disconnect();
                } catch (Exception ignored) {
                }
            }
        }
		*/
        @Override
        public void destroyObject(PooledObject<Jedis> pooledJedis) throws Exception {
            if (pooledJedis != null) {
                final BinaryJedis jedis = pooledJedis.getObject();
                if (jedis.isConnected()) {
                    try {
                        try {
                            jedis.quit();
                        } catch (Exception e) {
                        }
                        jedis.disconnect();
                    } catch (Exception e) {
                    }
                }
            }
        }

        @Override
        public boolean validateObject(final PooledObject<Jedis> obj) {
            try {
                Jedis jedis = obj.getObject();
                return "PONG".equals(jedis.ping());
            } catch (Exception ex) {
                return false;
            }
        }

        @Override
        public Jedis create() throws Exception {
            JedisShardInfo jsi = null;
            if (shardIterator.hasNext()) {
                jsi = shardIterator.next();
            } else {
                shardIterator = this.shards.iterator();
                if (shardIterator.hasNext()) {
                    jsi = shardIterator.next();
                }
            }
            final Jedis jedis = new Jedis(jsi.getHost(), jsi.getPort());
            // TODO jedis.connect();
            return jedis;
        }

        @Override
        public PooledObject<Jedis> wrap(Jedis jedis) {
            return new DefaultPooledObject<>(jedis);
        }
    }
}