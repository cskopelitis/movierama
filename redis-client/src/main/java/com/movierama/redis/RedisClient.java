package com.movierama.redis;

import com.movierama.redis.exception.RedisStoreException;
import com.movierama.redis.types.*;
import javolution.util.FastMap;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.Tuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author cskopelitis
 * @since 1.0
 */
public final class RedisClient<O> extends RedisCluster<O> implements RedisStore, RedisSet, RedisOrderedSet, RedisHash, RedisOrderedHash {

    //~~~~~~~~~~~~~~~~~~~~~ String
    public Set<String> keys(String pattern) {
        Jedis jedis = getRoPool().getResource();
        try {
            return jedis.keys(pattern);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public String get(String key) {
        Jedis jedis = getRoPool().getResource();
        try {
            return jedis.get(key);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public void set(String key, String value) {
        Jedis jedis = getRwPool().getResource();
        try {
            jedis.set(key, value);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void setTtl(String key, int seconds) {
        Jedis jedis = getRwPool().getResource();
        try {
            jedis.expire(key, seconds);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void delete(String key) {
        Jedis jedis = getRwPool().getResource();
        try {
            jedis.getClient().setTimeoutInfinite();
            jedis.del(key);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public boolean exists(String key) {
        Jedis jedis = getRoPool().getResource();
        try {
            return jedis.exists(key);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~ OrderedSet
    public void add(String setId, String member, Double score) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            jedis.zadd(setId, score, member);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchAdd(String setId, Map<String, Double> membersWithScore) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (Map.Entry<String, Double> memberWithScore : membersWithScore.entrySet()) {
                pipe.zadd(setId, memberWithScore.getValue(), memberWithScore.getKey());
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void incrScore(String setId, Long value, String member) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            jedis.zincrby(setId, value, member);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public Set<Tuple> rangeWithScores(String setId, long min, long max) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.zrangeWithScores(setId, min, max);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public Double score(String setId, String member) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.zscore(setId, member);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public Long rank(String setId, String member) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.zrank(setId, member);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public Set<String> range(String setId, Double min, Double max, boolean asc) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            if (asc) {
                return jedis.zrangeByScore(setId, min, max);
            } else {
                return jedis.zrevrangeByScore(setId, min, max);
            }
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public Long rangeRemove(String setId, Double min, Double max) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            return jedis.zremrangeByScore(setId, min, max);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void rangeRemove(String setId, Set<String> keys) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (String key : keys) {
                pipe.zrem(setId, key);
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public Set<String> select(String setId, Double min, Double max) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            LOGGER.debug(String.format("select %s between %s and %s", setId, min, max));
            if (LOGGER.isDebugEnabled()) {
                long count = jedis.zcount(setId, min.doubleValue(), max.doubleValue());
                LOGGER.debug(String.format("==> found %s records matching the criteria", count));
            }
            return jedis.zrangeByScore(setId, min.doubleValue(), max.doubleValue());
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~ Set
    public String pop(String setId) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            return jedis.spop(setId);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void add(String setId, String member) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            jedis.sadd(setId, member);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void addAll(String setId, Set<String> members) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (String member : members) {
                pipe.sadd(setId, member);
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Set<String> members(String setId) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.smembers(setId);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public boolean in(String setId, String member) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.sismember(setId, member);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public long length(String setId) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.scard(setId);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~ Hash

    public void set(String hashId, String fieldName, String value, boolean overwrite) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            if (overwrite) {
                jedis.hset(hashId, fieldName, value);
            } else {
                jedis.hsetnx(hashId, fieldName, value);
            }
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchSet(String hashId, Map<String, String> fieldsWithValues, boolean overwrite) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (Map.Entry<String, String> fieldWithValue : fieldsWithValues.entrySet()) {
                if (overwrite) {
                    pipe.hset(hashId, fieldWithValue.getKey(), fieldWithValue.getValue());
                } else {
                    pipe.hsetnx(hashId, fieldWithValue.getKey(), fieldWithValue.getValue());
                }
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchSet(Map<String, String> hashIdsWithValue, String fieldName) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (Map.Entry<String, String> hashIdWithValue : hashIdsWithValue.entrySet()) {
                pipe.hset(hashIdWithValue.getKey(), fieldName, hashIdWithValue.getValue());
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public String get(String hashId, String fieldName) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.hget(hashId, fieldName);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public void delete(String hashId, String fieldName) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            jedis.hdel(hashId, fieldName);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public Map<String, String> getAll(String hashId) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.hgetAll(hashId);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    public void incrBy(String hashId, String fieldName, Long value) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            jedis.hincrBy(hashId, fieldName, value);
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchIncrBy(String hashId, Set<String> fieldNames, Long value) {
        Jedis jedis = null;
        long t0 = System.currentTimeMillis();
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();

            for (String fieldName : fieldNames) {
                pipe.hincrBy(hashId, fieldName, value);
            }
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(String.format("Pipelining took %s ms",
                                           System.currentTimeMillis() - t0));
            }
            t0 = System.currentTimeMillis();

            pipe.sync();

            if (LOGGER.isTraceEnabled()) {
                LOGGER.debug(String.format("Batch insert (pipelined) of %s record(s) in %s ms",
                                           fieldNames.size(),
                                           System.currentTimeMillis() - t0));
            }
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchIncrBy(Set<String> hashIds, String fieldName, Long value) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            for (String hashId : hashIds) {
                pipe.hincrBy(hashId, fieldName, value);
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public long size(String hashId) {
        Jedis jedis = null;
        try {
            jedis = getRoPool().getResource();
            return jedis.hlen(hashId);
        } catch (Exception e) {
            handlePoolError(getRoPool(), jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRoPool().returnResource(jedis);
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~ RedisOrderedHash

    public Map<String, String> getFields(String hashId, String hashIndexId, Long min, Long max) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            Response<Set<String>> indexRes = pipe.zrangeByScore(hashIndexId, min.doubleValue(), max.doubleValue());
            pipe.sync();
            Set<String> fieldNames = indexRes.get();

            if (fieldNames.size() == 0) {
                return new HashMap<String, String>();
            }

            Response<List<String>> hashRes = pipe.hmget(hashId, fieldNames.toArray(new String[fieldNames.size()]));

            pipe.sync();
            List<String> values = hashRes.get();

            Map<String, String> ret = new FastMap<String, String>();
            int index = 0;
            for (String fieldName : fieldNames) {
                ret.put(fieldName, values.get(index++));
            }
            return ret;
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void incrBy(String hashId, String hashIndexId, String field, Double score, Long value) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();
            pipe.hincrBy(hashId, field, value);
            pipe.zadd(hashIndexId, score, field);
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchIncrBy(String hashId, String hashIndexId, Map<String, Double> fieldNamesWithScores, Long value) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();

            for (Map.Entry<String, Double> fieldNameWithScore : fieldNamesWithScores.entrySet()) {
                pipe.hincrBy(hashId, fieldNameWithScore.getKey(), value);
                pipe.zadd(hashIndexId, fieldNameWithScore.getValue(), fieldNameWithScore.getKey());
            }

            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }

    public void batchDelete(String hashId, String hashIndexId, Set<String> fieldNames, Long min, Long max) {
        Jedis jedis = null;
        try {
            jedis = getRwPool().getResource();
            Pipeline pipe = jedis.pipelined();

            for (String fieldName : fieldNames) {
                pipe.hdel(hashId, fieldName);
                pipe.zrem(hashIndexId, fieldName);
            }
            pipe.sync();
        } catch (Exception e) {
            handlePoolError(jedis);
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                getRwPool().returnResource(jedis);
            }
        }
    }
}
