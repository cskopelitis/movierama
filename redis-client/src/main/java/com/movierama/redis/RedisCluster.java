package com.movierama.redis;

import com.movierama.redis.exception.RedisStoreException;
import com.movierama.redis.model.RedisClusterState;
import com.movierama.redis.model.RedisNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.util.Pool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author cskopelitis
 * @version 2.0
 * @since 1.0.0
 */
class RedisCluster<O> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(RedisCluster.class);

    private final JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

    private String host;
    private String port;
    private RedisClusterState redisClusterState;

    private Pool<Jedis> roPool;
    private Pool<Jedis> rwPool;

    private final ReentrantReadWriteLock roLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.ReadLock roRl = roLock.readLock();
    private final ReentrantReadWriteLock.WriteLock roWl = roLock.writeLock();
    private final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.ReadLock rwRl = rwLock.readLock();
    private final ReentrantReadWriteLock.WriteLock rwWl = rwLock.writeLock();

    public long count() {
        Jedis jedis = roPool.getResource();
        try {
            return jedis.dbSize();
        } catch (Exception e) {
            roPool.returnBrokenResource(jedis);
            jedis = null;
            throw new RedisStoreException(e);
        } finally {
            if (jedis != null) {
                roPool.returnResource(jedis);
            }
        }
    }

    public void initClusterPools() {
        LOGGER.info("Initializing Redis Client v2...");

        RedisNode masterNode = new RedisNode(host, Integer.valueOf(port));
        redisClusterState = new RedisClusterState();
        redisClusterState.setMaster(masterNode);
        redisClusterState.setSlaves(Collections.<RedisNode>emptySet());

        initializeReadWritePool(redisClusterState.getMaster());
        initializeReadPool(redisClusterState.getMaster(), redisClusterState.getSlaves());
    }

    private void initializeReadWritePool(RedisNode redisNode) {
        try {
            rwWl.lock();
            // release any resources
            if (rwPool != null) this.rwPool.destroy();
            LOGGER.info("Initializing the RW Pool...");
            // recreate the pool
            this.rwPool = new JedisPool(jedisPoolConfig, redisNode.getHost(), redisNode.getPort());
        } finally {
            rwWl.unlock();
        }
    }

    private void initializeReadPool(RedisNode masterNode, Set<RedisNode> slaves) {
        try {
            roWl.lock();
            // release any resources
            if (roPool != null) this.roPool.destroy();
            LOGGER.info("Initializing the RO Pool...");
            // add all slaves
            List<JedisShardInfo> shards = new ArrayList<JedisShardInfo>();
            for (RedisNode slaveNodeInfo : slaves) {
                JedisShardInfo jedisShardInfo = createShardInstance(slaveNodeInfo);
                shards.add(jedisShardInfo);
            }
            // add master node too
            JedisShardInfo jedisShardInfo = createShardInstance(masterNode);
            shards.add(jedisShardInfo);
            LOGGER.info("Initialized a read pool of {} node(s)", shards.size());
            roPool = new RedisRoundRobinPool(jedisPoolConfig, shards);
        } finally {
            roWl.unlock();
        }
    }

    protected void handlePoolError(Jedis yoda) {
        if (yoda != null) {
            rwPool.returnBrokenResource(yoda);
            if (!checkOnline(yoda)) {
                initializeReadWritePool(redisClusterState.getMaster());
            }
        } else {
            initializeReadWritePool(redisClusterState.getMaster());
        }
    }

    protected void handlePoolError(Pool<Jedis> pool, Jedis jedi) {
        if (jedi != null) {
            pool.returnBrokenResource(jedi);

            initializeReadPool(redisClusterState.getMaster(), redisClusterState.getSlaves());
        }
    }

    private boolean checkOnline(final Jedis jedis) {
        try {
            try {
                jedis.connect();
                jedis.ping();
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (JedisConnectionException jce) {
            LOGGER.error("Error connecting to node", jce);
            return false;
        }
    }

    private JedisShardInfo createShardInstance(RedisNode redisNode) {
        return new JedisShardInfo(redisNode.getHost(), redisNode.getPort());
    }

    protected Pool<Jedis> getRoPool() {
        try {
            roRl.lock();
            return roPool;
        } finally {
            roRl.unlock();
        }
    }

    protected Pool<Jedis> getRwPool() {
        try {
            rwRl.lock();
            return rwPool;
        } finally {
            rwRl.unlock();
        }
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(String port) {
        this.port = port;
    }
}