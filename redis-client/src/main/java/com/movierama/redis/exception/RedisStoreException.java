package com.movierama.redis.exception;

/**
 * @author cskopelitis
 * @since 1.1
 */
public class RedisStoreException extends RuntimeException {

    public RedisStoreException() {
    }

    public RedisStoreException(String s) {
        super(s);
    }

    public RedisStoreException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public RedisStoreException(Throwable throwable) {
        super(throwable);
    }
}
