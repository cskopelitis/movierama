# README #
The below steps are necessary to compile and run the MovieRama Demo application

# Prerequisites #
* Java 1.7
* Redis 3.0.6 or higher

# Configuration #
All the required properties are located in `movierama.properties`. Copy the sample file provided `cache/src/resources/movierama.properties` to a desired location `PATH_TO_PROPERTIES` and then
pass it as an argument to the JVM

## JVM arguments ##
`-Xmx128m -Dapp.config=$PATH_TO_PROPERTIES`

# Run Application #
* Copy to $TOMCAT_HOME/webapps
* Start Tomcat `$TOMCAT_HOME/bin/startup.sh`

# Web Console #
* The console can be viewed at http://localhost:8080/movierama