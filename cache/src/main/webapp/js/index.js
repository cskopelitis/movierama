client = new MovieRamaClient();

function search(q) {
    clearResults();
    client.search(q, populateResults, function (response, status, xhr) {
        alert(response);
    });
}

function getNowPlaying() {
    clearResults();
    client.getPlayingNow(populateResults, function (response, status, xhr) {
    });
}

function populateResults(results) {
    clearResults();

    $.each(results, function (key, movie) {
        $('#resultsTable').append('<tr><td><h2>'
                + movie.title
                + '</h2></td></tr><tr><td>'
                + movie.releaseYear
                + ' - Starring: '
                + movie.cast
                + '</td></tr><tr><td>'
                + movie.synopsis
                + '</td></tr><tr><td>'
                + movie.reviews
                + ' Reviews</td></tr>');
    });
}

function clearResults() {
    var resultsTable = $("#resultsTable");
    resultsTable.find("tr:gt(0)").remove();
    resultsTable.find("tr:eq(0)").remove();
}

function getMoviePrototype() {
    return {
        title: "", releaseYear: "", cast: [], synopsis: "", reviews: ""
    }
}