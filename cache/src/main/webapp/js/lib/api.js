function MovieRamaClient() {
}

MovieRamaClient.prototype.search = function (q, onSuccess, onError) {
    var url = SEARCH_URL.replace("{0}", q);
    $.ajax({
        cache: false, type: "GET", url: url, success: onSuccess, error: onError
    })
};

MovieRamaClient.prototype.getPlayingNow = function (onSuccess, onError) {
    $.ajax({
        cache: false, type: "GET", url: PLAYING_NOW_URL, success: onSuccess, error: onError
    })
};