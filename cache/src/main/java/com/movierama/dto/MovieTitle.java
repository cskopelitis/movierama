package com.movierama.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cskopelitis
 * @since 08/04/2016
 */
public class MovieTitle implements Serializable {

    private String title;
    private Map<String, String> ids = new HashMap<>();

    @Override
    public String toString() {
        return "MovieTitle{" +
                "title='" + title + '\'' +
                ", ids=" + ids.values() +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, String> getIds() {
        return ids;
    }

    public void setIds(Map<String, String> ids) {
        this.ids = ids;
    }
}
