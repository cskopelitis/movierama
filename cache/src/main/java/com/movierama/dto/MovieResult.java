package com.movierama.dto;

import java.util.List;
import java.util.Objects;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class MovieResult extends MovieTitle {

    private String releaseYear;
    private List<String> cast;
    private String synopsis;
    private int reviews;

    @Override
    public String toString() {
        return "MovieResult{" +
                "title='" + getTitle() + '\'' +
                ", releaseYear='" + releaseYear + '\'' +
                ", cast=" + cast +
                ", synopsis='" + synopsis + '\'' +
                ", reviews=" + reviews +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieResult that = (MovieResult) o;
        return Objects.equals(getTitle(), getTitle()) &&
                Objects.equals(getReviews(), that.getReviews()) &&
                Objects.equals(getReleaseYear(), that.getReleaseYear()) &&
                Objects.equals(getCast().size(), that.getCast().size()) &&
                Objects.equals(getSynopsis(), that.getSynopsis());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getReleaseYear(), getCast(), getSynopsis(), getReviews());
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public List<String> getCast() {
        return cast;
    }

    public void setCast(List<String> cast) {
        this.cast = cast;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }
}
