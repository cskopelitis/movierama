package com.movierama.web;

import com.movierama.dto.MovieResult;
import com.movierama.service.search.SearchService;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.web.util.JsonObjectMapperUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/search")
public class MovieSearchResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieSearchResource.class);

    @Autowired
    private SearchService searchService;

    private final ObjectMapper objectMapper = new JsonObjectMapperUtils();

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> searchMovie(@RequestParam(value = "q") String searchString,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "forceRefresh", required = false, defaultValue = "0") boolean forceRefresh) {
        LOGGER.debug("[GET] /search?q={}&?page={}&forceRefresh={}", searchString, page, forceRefresh);

        try {
            List<MovieResult> movieResults = searchService.search(
                    searchString != null ? searchString.trim() : null,
                    page,
                    forceRefresh);
            if (movieResults.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.ACCEPTED);
            } else {
                return new ResponseEntity<>(objectMapper.writeValueAsString(movieResults), HttpStatus.OK);
            }
        } catch (SearchServiceException sse) {
            LOGGER.error("An error occurred while processing the request", sse);
            return new ResponseEntity<>(sse.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOGGER.error("An unexpected error occurred while processing the request", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/now", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getNowPlaying() {
        LOGGER.debug("[GET] /search/now");
        try {
            List<MovieResult> nowPlaying = searchService.getNowPlaying();
            return new ResponseEntity<>(objectMapper.writeValueAsString(nowPlaying), HttpStatus.OK);
        } catch (SearchServiceException sse) {
            LOGGER.error("An error occurred while processing the request", sse);
            return new ResponseEntity<>(sse.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOGGER.error("An unexpected error occurred while processing the request", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
