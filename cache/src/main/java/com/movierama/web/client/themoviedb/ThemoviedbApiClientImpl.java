package com.movierama.web.client.themoviedb;

import com.movierama.web.client.GenericWebserviceClient;
import com.movierama.web.client.ThemoviedbApiClient;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovie;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovieList;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Component("themoviedbApiClient")
public class ThemoviedbApiClientImpl extends GenericWebserviceClient implements ThemoviedbApiClient {

    private static final String API_KEY = "186b266209c2da50f898b7977e2a44dd";

    private static final String PARAM_API_KEY = "api_key";
    private static final String PARAM_QUERY = "query";
    private static final String PARAM_APPEND = "append_to_response";
    private static final String PARAM_PAGE = "page";

    @Override
    protected String buildBaseUrl() {
        return "http://api.themoviedb.org/3";
    }

    @Override
    public ThemoviedbMovieList searchMovie(final String queryString, final int page) {
        return httpGet("search/movie", new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
            put(PARAM_QUERY, queryString);
            put(PARAM_PAGE, String.valueOf(page));
        }}, ThemoviedbMovieList.class);
    }

    @Override
    public ThemoviedbMovieList getNowPlaying() {
        return httpGet("movie/now_playing", new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
        }}, ThemoviedbMovieList.class);
    }

    @Override
    public ThemoviedbMovie getMovieInfo(String movieId) {
        return httpGet(String.format("movie/%s", movieId), new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
            put(PARAM_APPEND, "reviews,credits");
        }}, ThemoviedbMovie.class);
    }
}
