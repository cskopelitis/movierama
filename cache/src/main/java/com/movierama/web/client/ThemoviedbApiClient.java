package com.movierama.web.client;

import com.movierama.web.client.themoviedb.json.ThemoviedbMovie;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovieList;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public interface ThemoviedbApiClient {

    ThemoviedbMovieList searchMovie(String queryString, int page);

    ThemoviedbMovieList getNowPlaying();

    ThemoviedbMovie getMovieInfo(String movieId);
}
