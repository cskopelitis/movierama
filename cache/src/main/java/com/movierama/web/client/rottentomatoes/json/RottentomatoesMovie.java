package com.movierama.web.client.rottentomatoes.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RottentomatoesMovie extends RottentomatoesMovieTitle {

    private String year;
    private String synopsis;
    private List<RottentomatoesMovieCast> cast;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    @JsonProperty("abridged_cast")
    public List<RottentomatoesMovieCast> getCast() {
        return cast;
    }

    @JsonProperty("abridged_cast")
    public void setCast(List<RottentomatoesMovieCast> cast) {
        this.cast = cast;
    }
}
