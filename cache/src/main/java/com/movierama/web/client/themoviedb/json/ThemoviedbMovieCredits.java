package com.movierama.web.client.themoviedb.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThemoviedbMovieCredits {

    private List<ThemoviedbMovieCast> cast;

    public List<ThemoviedbMovieCast> getCast() {
        return cast;
    }

    public void setCast(List<ThemoviedbMovieCast> cast) {
        this.cast = cast;
    }
}
