package com.movierama.web.client.rottentomatoes.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RottentomatoesMovieReviews {

    private int totalReviews;

    @JsonProperty("total")
    public int getTotalReviews() {
        return totalReviews;
    }

    @JsonProperty("total")
    public void setTotalReviews(int totalReviews) {
        this.totalReviews = totalReviews;
    }
}
