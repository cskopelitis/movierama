package com.movierama.web.client.rottentomatoes;

import com.movierama.web.client.GenericWebserviceClient;
import com.movierama.web.client.RottentomatoesApiClient;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovie;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieList;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieReviews;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Component("rottentomatoesApiClient")
public class RottentomatoesApiClientImpl extends GenericWebserviceClient implements RottentomatoesApiClient {

    private static final String API_KEY = "qtqep7qydngcc7grk4r4hyd9";

    private static final String PARAM_API_KEY = "apikey";
    private static final String PARAM_QUERY = "q";
    private static final String PARAM_PAGE_LIMIT = "page_limit";
    private static final String PARAM_PAGE = "page";

    @Override
    protected String buildBaseUrl() {
        return "http://api.rottentomatoes.com/api/public/v1.0";
    }

    @Override
    public RottentomatoesMovieList searchMovie(final String queryString, final int page) {
        return httpGet("movies.json", new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
            put(PARAM_QUERY, queryString);
            put(PARAM_PAGE_LIMIT, "10");
            put(PARAM_PAGE, String.valueOf(page));
        }}, RottentomatoesMovieList.class);
    }

    @Override
    public RottentomatoesMovieList getNowPlaying() {
        return httpGet("lists/movies/in_theaters.json", new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
        }}, RottentomatoesMovieList.class);
    }

    @Override
    public RottentomatoesMovie getMovieInfo(String movieId) {
        return httpGet(String.format("movies/%s.json", movieId), new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
        }}, RottentomatoesMovie.class);
    }

    @Override
    public RottentomatoesMovieReviews getMovieReviews(String movieId) {
        return httpGet(String.format("movies/%s/reviews.json", movieId), new HashMap<String, String>() {{
            put(PARAM_API_KEY, API_KEY);
        }}, RottentomatoesMovieReviews.class);
    }
}
