package com.movierama.web.client.themoviedb.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThemoviedbMovie extends ThemoviedbMovieTitle {

    private String releaseDate;
    private String overview;
    private ThemoviedbMovieCredits credits;
    private ThemoviedbMovieReviews reviews;

    @JsonProperty("release_date")
    public String getReleaseDate() {
        return releaseDate;
    }

    @JsonProperty("release_date")
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public ThemoviedbMovieCredits getCredits() {
        return credits;
    }

    public void setCredits(ThemoviedbMovieCredits credits) {
        this.credits = credits;
    }

    public ThemoviedbMovieReviews getReviews() {
        return reviews;
    }

    public void setReviews(ThemoviedbMovieReviews reviews) {
        this.reviews = reviews;
    }
}
