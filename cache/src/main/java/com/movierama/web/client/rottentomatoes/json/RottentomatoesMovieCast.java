package com.movierama.web.client.rottentomatoes.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RottentomatoesMovieCast {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
