package com.movierama.web.client;

import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovie;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieList;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieReviews;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public interface RottentomatoesApiClient {

    RottentomatoesMovieList searchMovie(String queryString, int page);

    RottentomatoesMovieList getNowPlaying();

    RottentomatoesMovie getMovieInfo(String movieId);

    RottentomatoesMovieReviews getMovieReviews(String movieId);
}
