package com.movierama.web.client;

import com.movierama.web.util.JsonObjectMapperUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.util.Map;

/**
 * @author cskopelitis
 * @since 1.0
 */
public abstract class GenericWebserviceClient {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GenericWebserviceClient.class);

    protected WebResource resource;

    protected final ObjectMapper objectMapper = new JsonObjectMapperUtils();

    @PostConstruct
    public void initialize() {
        this.resource = Client.create().resource(buildBaseUrl());
    }

    protected abstract String buildBaseUrl();

    protected <E> E httpGet(String path, Map<String, String> queryParameters, Class<?> clazz) {
        try {
            WebResource webResource = resource.path(path);

            for (Map.Entry<String, String> queryParameter : queryParameters.entrySet()) {
                // url encode the parameter value
                webResource = webResource.queryParam(queryParameter.getKey(), queryParameter.getValue());
            }

            LOGGER.debug(String.format("Executing [GET] request on %s", webResource.toString()));

            ClientResponse response = webResource.get(ClientResponse.class);

            return extractResponseEntity(response, clazz);
        } catch (IOException ioe) {
            throw new IllegalArgumentException(ioe);
        }
    }

    @SuppressWarnings("unchecked")
    protected <E> E extractResponseEntity(ClientResponse response, Class<?> clazz) throws IOException {
        if (response.getStatus() == HttpURLConnection.HTTP_OK) {
            if (response.hasEntity()) {
                E entity;
                if (!clazz.equals(String.class)) {
                    entity = (E) objectMapper.readValue(response.getEntity(String.class), clazz);
                }
                // if the response is plain text
                else {
                    entity = (E) response.getEntity(clazz);
                }
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Response received with body {}", ((JsonObjectMapperUtils) objectMapper).writeValueAsString(entity, false));
                }
                return entity;
            }
        } else {
            throw new ConnectException(String.format("Webservice call returned %s", response.getStatus()));
        }
        return null;
    }
}
