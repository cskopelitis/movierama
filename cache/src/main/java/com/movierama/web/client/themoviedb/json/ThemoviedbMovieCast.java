package com.movierama.web.client.themoviedb.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThemoviedbMovieCast {

    private String name;
    private String order;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
