package com.movierama.web.client.themoviedb.json;

/**
 * @author cskopelitis
 * @since 08/04/2016
 */
public class ThemoviedbMovieReviewsResponse {

    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
