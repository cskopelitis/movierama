package com.movierama.web.client.themoviedb.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThemoviedbMovieList {

    private List<ThemoviedbMovieTitle> results;

    public List<ThemoviedbMovieTitle> getResults() {
        return results;
    }

    public void setResults(List<ThemoviedbMovieTitle> results) {
        this.results = results;
    }
}
