package com.movierama.web.client.rottentomatoes.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RottentomatoesMovieList {

    private int total;
    private List<RottentomatoesMovieTitle> movies;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RottentomatoesMovieTitle> getMovies() {
        return movies;
    }

    public void setMovies(List<RottentomatoesMovieTitle> movies) {
        this.movies = movies;
    }
}
