package com.movierama.web.client.themoviedb.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThemoviedbMovieReviews {

    private int totalReviews;

    @JsonProperty("total_results")
    public int getTotalReviews() {
        return totalReviews;
    }

    @JsonProperty("total_results")
    public void setTotalReviews(int totalReviews) {
        this.totalReviews = totalReviews;
    }
}
