package com.movierama.web.util;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;

/** @author christos.skopelitis */
public class JsonObjectMapperUtils extends ObjectMapper {

    public JsonObjectMapperUtils() {

        // Serialization Configuration
        configure(SerializationConfig.Feature.USE_ANNOTATIONS, true);
        configure(SerializationConfig.Feature.AUTO_DETECT_GETTERS, true);
        configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, false);
        configure(SerializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS, true);
        configure(SerializationConfig.Feature.USE_STATIC_TYPING, false);
        configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);

        // Deserialization Configuration
        configure(DeserializationConfig.Feature.USE_ANNOTATIONS, true);
        configure(DeserializationConfig.Feature.AUTO_DETECT_SETTERS, true);
        configure(DeserializationConfig.Feature.AUTO_DETECT_CREATORS, false);
        configure(DeserializationConfig.Feature.AUTO_DETECT_FIELDS, false);
        configure(DeserializationConfig.Feature.USE_GETTERS_AS_SETTERS, true);
        configure(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS, true);
        configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // Other
        setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
    }

    public <T> T in(InputStream ist, Class<T> type) throws IOException {
        return readValue(ist, type);
    }

    public <T> void out(OutputStream ost, T data) throws IOException {
        JsonGenerator ww = getJsonFactory().createJsonGenerator(ost, JsonEncoding.UTF8);
        ww.useDefaultPrettyPrinter();
        writeValue(ww, data);
    }

    /**
     * Method that can be used to serialize any Java value as
     * a String. Functionally equivalent to calling
     * {@link #writeValue(Writer, Object)} with {@link java.io.StringWriter}
     * and constructing String, but more efficient.
     *
     * @param value  the Java value to convert to json string
     * @param pretty true to pass through prettyPrinter
     */
    public String writeValueAsString(Object value, boolean pretty) throws IOException, JsonGenerationException, JsonMappingException {
        return pretty ? writerWithDefaultPrettyPrinter().writeValueAsString(value) : writeValueAsString(value);
    }
}