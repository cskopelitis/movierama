package com.movierama.cache;

import com.movierama.dto.MovieTitle;

import java.util.List;

/**
 * @author cskopelitis
 * @since 10/04/2016
 */
public interface NowPlayingCache {

    String CACHE_ID = "nowPlaying";

    void cacheNowPlaying(List<MovieTitle> nowPlayingList);

    List<MovieTitle> getNowPlaying();
}
