package com.movierama.cache.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author cskopelitis
 * @since 10/04/2016
 */
public class Md5Hash {
    private static final Logger LOGGER = LoggerFactory.getLogger(Md5Hash.class);

    private Md5Hash() {
    }

    public static String hashValue(String value) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(value.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hash = number.toString(16);
            LOGGER.debug("Digested '{}' to {}", value, hash);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
