package com.movierama.cache;

import com.movierama.dto.MovieResult;

/**
 * @author cskopelitis
 * @since 10/04/2016
 */
public interface MovieCache {

    void cacheMovie(MovieResult movieResult);

    MovieResult getMovie(String title);
}
