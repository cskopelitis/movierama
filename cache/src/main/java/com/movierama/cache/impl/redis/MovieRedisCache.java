package com.movierama.cache.impl.redis;

import com.movierama.cache.Cache;
import com.movierama.cache.MovieCache;
import com.movierama.cache.utils.Md5Hash;
import com.movierama.dto.MovieResult;
import com.movierama.redis.types.RedisHash;
import com.movierama.redis.types.RedisStore;
import com.movierama.web.util.JsonObjectMapperUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Service
public class MovieRedisCache implements Cache<MovieResult>, MovieCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieRedisCache.class);

    private static final String VALUE = "json";

    @Autowired
    private RedisHash redisHash;
    @Autowired
    private RedisStore redisStore;

    @Value("${cache.ttl.seconds}")
    private int ttl;

    private final ObjectMapper objectMapper = new JsonObjectMapperUtils();

    @Override
    public MovieResult get(String key) {
        String movieJson = redisHash.get(Md5Hash.hashValue(key.toLowerCase()), VALUE);
        if (movieJson != null) {
            try {
                return objectMapper.readValue(movieJson, MovieResult.class);
            } catch (IOException e) {
                LOGGER.error("An unexpected error occurred while deserializing {}", movieJson, e);
                throw new IllegalArgumentException(e);
            }
        }
        return null;
    }

    @Override
    public void add(String key, MovieResult movieResult) {
        try {
            String movieJson = objectMapper.writeValueAsString(movieResult);
            String movieCacheKey = Md5Hash.hashValue(key.toLowerCase());
            redisHash.set(movieCacheKey, VALUE, movieJson, true);
            if (ttl > 0) {
                redisStore.setTtl(movieCacheKey, ttl);
            }
        } catch (IOException e) {
            LOGGER.error("An unexpected error occurred while serializing {}", movieResult, e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void cacheMovie(MovieResult movieResult) {
        add(movieResult.getTitle(), movieResult);
    }

    @Override
    public MovieResult getMovie(String title) {
        return get(title);
    }
}
