package com.movierama.cache.impl.redis;

import com.movierama.cache.Cache;
import com.movierama.cache.NowPlayingCache;
import com.movierama.cache.SearchResultsCache;
import com.movierama.cache.utils.Md5Hash;
import com.movierama.dto.MovieTitle;
import com.movierama.redis.types.RedisSet;
import com.movierama.redis.types.RedisStore;
import com.movierama.web.util.JsonObjectMapperUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Service
public class CollectionRedisCache implements Cache<List<MovieTitle>>, NowPlayingCache, SearchResultsCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionRedisCache.class);

    @Autowired
    private RedisSet redisSet;
    @Autowired
    private RedisStore redisStore;

    @Value("${cache.ttlMinutes.minutes:0}")
    private int ttlMinutes;

    private final ObjectMapper objectMapper = new JsonObjectMapperUtils();

    @Override
    public List<MovieTitle> get(String listId) {
        List<MovieTitle> movieTitles = new ArrayList<>();
        try {
            Set<String> items = redisSet.members(listId);
            for (String jsonMovieTitle : items) {
                movieTitles.add(objectMapper.readValue(jsonMovieTitle, MovieTitle.class));
            }
            return movieTitles;
        } catch (IOException e) {
            LOGGER.error("An unexpected error occurred while deserializing {}", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void add(String listId, List<MovieTitle> movieTitles) {
        try {
            Set<String> jsonMovieList = new HashSet<>();
            for (MovieTitle movieTitle : movieTitles) {
                jsonMovieList.add(objectMapper.writeValueAsString(movieTitle));
            }
            redisSet.addAll(listId, jsonMovieList);
            if (ttlMinutes > 0) {
                redisStore.setTtl(listId, ttlMinutes);
            }
        } catch (IOException e) {
            LOGGER.error("An unexpected error occurred while serializing", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void cacheNowPlaying(List<MovieTitle> nowPlayingList) {
        add(NowPlayingCache.CACHE_ID, nowPlayingList);
    }

    @Override
    public List<MovieTitle> getNowPlaying() {
        return get(NowPlayingCache.CACHE_ID);
    }

    @Override
    public void cacheSearchResults(String queryString, List<MovieTitle> results) {
        add(Md5Hash.hashValue(SearchResultsCache.CACHE_ID + queryString.toLowerCase()), results);
    }

    @Override
    public List<MovieTitle> getSearchResults(String queryString) {
        return get(Md5Hash.hashValue(SearchResultsCache.CACHE_ID + queryString.toLowerCase()));
    }

    @Override
    public void flush(String queryString) {
        redisStore.delete(Md5Hash.hashValue(SearchResultsCache.CACHE_ID + queryString.toLowerCase()));
    }
}
