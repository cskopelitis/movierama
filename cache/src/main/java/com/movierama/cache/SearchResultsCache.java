package com.movierama.cache;

import com.movierama.dto.MovieTitle;

import java.util.List;

/**
 * @author cskopelitis
 * @since 10/04/2016
 */
public interface SearchResultsCache {

    String CACHE_ID = "search";

    void cacheSearchResults(String queryString, List<MovieTitle> results);

    List<MovieTitle> getSearchResults(String queryString);

    void flush(String queryString);
}
