package com.movierama.cache;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public interface Cacheable {

    String getKey();
}
