package com.movierama.cache;

/**
 * @author cskopelitis
 * @since 14/09/2015
 */
public interface Cache<V> {

    V get(String key);

    void add(String key, V value);
}
