package com.movierama.service.search;

import com.movierama.dto.MovieResult;
import com.movierama.service.search.exception.SearchServiceException;

import java.util.List;

public interface SearchService {

    List<MovieResult> search(String queryString, int page, boolean forceRefresh) throws SearchServiceException;

    List<MovieResult> getNowPlaying() throws SearchServiceException;
}
