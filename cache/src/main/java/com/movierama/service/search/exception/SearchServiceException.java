package com.movierama.service.search.exception;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class SearchServiceException extends Exception {

    public SearchServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
