package com.movierama.service.search.impl;

import com.movierama.cache.NowPlayingCache;
import com.movierama.cache.SearchResultsCache;
import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchService;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Service("searchService")
public class SearchServiceImpl extends ParallelExecutorService<MovieResult> implements SearchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Autowired
    @Qualifier("collectionRedisCache")
    private SearchResultsCache searchResultsCache;
    @Autowired
    @Qualifier("collectionRedisCache")
    private NowPlayingCache nowPlayingCache;
    @Autowired
    private MovieDetailsSearchManagementService movieDetailsSearchManagementService;
    @Autowired
    private MovieSearchManagementService movieSearchManagementService;

    public SearchServiceImpl() {
        super(DEFAULT_THREADS * 4, DEFAULT_TIMEOUT_MILLIES);
    }

    @Override
    public List<MovieResult> search(String queryString, int page, boolean forceRefresh) throws SearchServiceException {
        if (queryString == null || queryString.isEmpty()) {
            LOGGER.info("Empty string, nothing to search for");
            return Collections.emptyList();
        }

        long t0 = System.currentTimeMillis();
        if (forceRefresh) {
            LOGGER.info("Clearing cache for query '{}'", queryString);
            searchResultsCache.flush(queryString);
        }

        List<MovieResult> movieResults = new ArrayList<>();

        LOGGER.debug("Searching for '{}'", queryString);
        List<MovieTitle> results = searchResultsCache.getSearchResults(queryString);
        if (!results.isEmpty()) {
            LOGGER.debug("Found {} result items in cache", results.size());
        } else {
            results = movieSearchManagementService.search(queryString, page);
            LOGGER.debug("Search returned {} result(s)", results.size());

            searchResultsCache.cacheSearchResults(queryString, results);
        }

        for (MovieTitle movieTitle : results) {
            MovieResult movieResult = movieDetailsSearchManagementService.getMovie(movieTitle);
            if (movieResult != null) {
                movieResults.add(movieResult);
            }
        }

        LOGGER.info("Returned {} results in {} ms", movieResults.size(), System.currentTimeMillis() - t0);

        return movieResults;
    }

    @Override
    public List<MovieResult> getNowPlaying() throws SearchServiceException {
        long t0 = System.currentTimeMillis();

        List<MovieTitle> nowPlayingTitles = nowPlayingCache.getNowPlaying();

        if (nowPlayingTitles.isEmpty()) {
            LOGGER.debug("Calling remote systems...");
            nowPlayingTitles = movieSearchManagementService.getNowPlaying();
            nowPlayingCache.cacheNowPlaying(nowPlayingTitles);
        } else {
            LOGGER.debug("loaded items {} from cache", nowPlayingTitles.size());
        }

        long t1 = System.currentTimeMillis();
        List<MovieResult> nowPlayingMovies = new ArrayList<>();
        for (MovieTitle movieTitle : nowPlayingTitles) {
            LOGGER.debug("Getting details for {}", movieTitle.getTitle());
            MovieResult movieResult = movieDetailsSearchManagementService.getMovie(movieTitle);
            if (movieResult != null) {
                nowPlayingMovies.add(movieResult);
            }
        }
        LOGGER.debug("Retrieved details for {} movies in {} ms", nowPlayingMovies.size(), System.currentTimeMillis() - t1);

        // sort the results
        Collections.sort(nowPlayingMovies, new Comparator<MovieResult>() {
            @Override
            public int compare(MovieResult m1, MovieResult m2) {
                return m1.getTitle().compareTo(m2.getTitle());
            }
        });

        LOGGER.info("Returned {} results in {} ms", nowPlayingMovies.size(), System.currentTimeMillis() - t0);

        return nowPlayingMovies;
    }

    void setSearchResultsCache(SearchResultsCache searchResultsCache) {
        this.searchResultsCache = searchResultsCache;
    }

    void setNowPlayingCache(NowPlayingCache nowPlayingCache) {
        this.nowPlayingCache = nowPlayingCache;
    }

    void setMovieDetailsSearchManagementService(MovieDetailsSearchManagementService movieDetailsSearchManagementService) {
        this.movieDetailsSearchManagementService = movieDetailsSearchManagementService;
    }

    void setMovieSearchManagementService(MovieSearchManagementService movieSearchManagementService) {
        this.movieSearchManagementService = movieSearchManagementService;
    }
}
