package com.movierama.service.search;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.exception.SearchServiceException;

import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public interface SearchClient {

    String getId();

    List<MovieTitle> getNowPlaying() throws SearchServiceException;

    List<MovieTitle> search(String queryString, int page) throws SearchServiceException;

    MovieResult getMovie(MovieTitle movieTitle) throws SearchServiceException;
}
