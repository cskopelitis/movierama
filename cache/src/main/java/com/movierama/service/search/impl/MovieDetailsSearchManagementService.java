package com.movierama.service.search.impl;

import com.movierama.cache.MovieCache;
import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Service(MovieDetailsSearchManagementService.SERVICE_NAME)
class MovieDetailsSearchManagementService extends ParallelExecutorService<MovieResult> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieDetailsSearchManagementService.class);

    public static final String SERVICE_NAME = "movieDetailsSearchManagementService";

    @Autowired
    @Qualifier("movieRedisCache")
    private MovieCache movieCache;
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Autowired
    private Set<SearchClient> searchClients;

    public MovieDetailsSearchManagementService() {
        super(DEFAULT_THREADS, DEFAULT_TIMEOUT_MILLIES);
    }

    public MovieResult getMovie(final MovieTitle movieTitle) throws SearchServiceException {
        try {
            final MovieResult cachedMovieResult = movieCache.getMovie(movieTitle.getTitle());
            if (cachedMovieResult != null) {
                LOGGER.debug("Movie {} found in cache", movieTitle);
                return cachedMovieResult;
            }

            List<Callable<MovieResult>> tasks = new ArrayList<>();
            for (final SearchClient searchClient : searchClients) {
                tasks.add(new Callable<MovieResult>() {
                    @Override
                    public MovieResult call() {
                        try {
                            LOGGER.debug("Getting details for '{}' from {}", movieTitle, searchClient.getClass().getSimpleName());
                            return searchClient.getMovie(movieTitle);
                        } catch (SearchServiceException e) {
                            LOGGER.error("An error occurred while executing {}", searchClient.getClass().getSimpleName(), e);
                        } catch (Exception e) {
                            LOGGER.error("An unexpected error  occurred while executing {}", searchClient.getClass().getSimpleName(), e);
                        }
                        return null;
                    }
                });
            }

            List<MovieResult> results = execute(tasks);
            MovieResult mergedMovieResult = merge(results);
            movieCache.cacheMovie(mergedMovieResult);
            return mergedMovieResult;
        } catch (Exception e) {
            LOGGER.error("An unexpected error occurred while searching", e);
            throw new IllegalStateException(e);
        }
    }

    private MovieResult merge(List<MovieResult> movieResults) {
        if (movieResults.isEmpty()) {return null;}
        if (movieResults.size() == 1) {
            return movieResults.iterator().next();
        }

        MovieResult merged = new MovieResult();

        for (MovieResult movieResult : movieResults) {
            if (movieResult == null) {continue;}

            if (merged.getTitle() == null) {
                LOGGER.trace("Set Title {}", movieResult.getTitle());
                merged.setTitle(movieResult.getTitle());
            }
            if (merged.getReleaseYear() == null && movieResult.getReleaseYear() != null) {
                LOGGER.trace("Merge ReleaseYear {}", movieResult.getReleaseYear());
                merged.setReleaseYear(movieResult.getReleaseYear());
            }
            if (merged.getCast() == null && movieResult.getCast() != null) {
                LOGGER.trace("Merge Cast");
                merged.setCast(movieResult.getCast());
            } else if (merged.getCast() != null && movieResult.getCast() != null) {
                LOGGER.trace("Merge Cast {}+{}", merged.getCast().size(), movieResult.getCast().size());
                for (String castMember : movieResult.getCast()) {
                    if (!merged.getCast().contains(castMember)) {
                        merged.getCast().add(castMember);
                    }
                }
                LOGGER.trace("Merge Cast total={}", merged.getCast().size());
            }

            merged.setReviews(merged.getReviews() + movieResult.getReviews());

            if (merged.getSynopsis() == null && movieResult.getSynopsis() != null) {
                LOGGER.trace("Merge Synopsis {}", movieResult.getSynopsis());
                merged.setSynopsis(movieResult.getSynopsis());
            } else if (merged.getSynopsis() != null && movieResult.getSynopsis() != null) {
                if (merged.getSynopsis().length() < movieResult.getSynopsis().length()) {
                    LOGGER.trace("Replace Synopsis {}", movieResult.getSynopsis());
                    merged.setSynopsis(movieResult.getSynopsis());
                }
            }
        }
        return merged;
    }

    void setMovieCache(MovieCache movieCache) {
        this.movieCache = movieCache;
    }

    void setSearchClients(Set<SearchClient> searchClients) {
        this.searchClients = searchClients;
    }
}
