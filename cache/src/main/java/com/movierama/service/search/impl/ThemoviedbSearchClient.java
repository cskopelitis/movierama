package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.web.client.ThemoviedbApiClient;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovie;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovieCast;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovieList;
import com.movierama.web.client.themoviedb.json.ThemoviedbMovieTitle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Component("themoviedbSearchClient")
class ThemoviedbSearchClient implements SearchClient {

    public static final String ID = "themoviedb";

    @Autowired
    private ThemoviedbApiClient themoviedbApiClient;

    @Override
    public List<MovieTitle> search(String queryString, int page) throws SearchServiceException {
        try {
            ThemoviedbMovieList response = themoviedbApiClient.searchMovie(queryString, page);
            return parseResponse(response);
        } catch (Exception e) {
            throw new SearchServiceException(String.format("An error occurred during query '%s'", queryString), e);
        }
    }

    @Override
    public List<MovieTitle> getNowPlaying() throws SearchServiceException {
        try {
            ThemoviedbMovieList response = themoviedbApiClient.getNowPlaying();
            return parseResponse(response);
        } catch (Exception e) {
            throw new SearchServiceException("An error occurred while executing the call", e);
        }
    }

    @Override
    public MovieResult getMovie(MovieTitle movieTitle) throws SearchServiceException {
        String movieId = movieTitle.getIds().get(getId());
        try {
            if (movieId != null) {
                ThemoviedbMovie themoviedbMovie = themoviedbApiClient.getMovieInfo(movieId);
                return parseResponse(themoviedbMovie);
            }
            return null;
        } catch (Exception e) {
            throw new SearchServiceException(String.format("An error occurred searching for movie '%s'", movieId), e);
        }
    }

    private List<MovieTitle> parseResponse(ThemoviedbMovieList response) {
        List<MovieTitle> results = new ArrayList<>();

        for (ThemoviedbMovieTitle themoviedbMovieTitle : response.getResults()) {
            MovieTitle movieTitle = new MovieTitle();
            movieTitle.getIds().put(getId(), themoviedbMovieTitle.getId());
            movieTitle.setTitle(themoviedbMovieTitle.getTitle());
            results.add(movieTitle);
        }

        return results;
    }

    private MovieResult parseResponse(ThemoviedbMovie themoviedbMovie) throws ExecutionException, InterruptedException {
        final MovieResult movieResult = new MovieResult();

        movieResult.setTitle(themoviedbMovie.getTitle());
        movieResult.setSynopsis(themoviedbMovie.getOverview());
        String releaseDate = themoviedbMovie.getReleaseDate();
        if (releaseDate != null && !releaseDate.isEmpty()) {
            movieResult.setReleaseYear(releaseDate.substring(0, releaseDate.indexOf('-')));
        }

        movieResult.setCast(new ArrayList<String>());
        for (ThemoviedbMovieCast themoviedbMovieCast : themoviedbMovie.getCredits().getCast()) {
            movieResult.getCast().add(themoviedbMovieCast.getName());
        }

        movieResult.setReviews(themoviedbMovie.getReviews().getTotalReviews());

        return movieResult;
    }

    @Override
    public String getId() {
        return ID;
    }

    void setThemoviedbApiClient(ThemoviedbApiClient themoviedbApiClient) {
        this.themoviedbApiClient = themoviedbApiClient;
    }
}
