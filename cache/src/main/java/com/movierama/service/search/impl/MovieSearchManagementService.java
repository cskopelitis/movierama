package com.movierama.service.search.impl;

import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
@Service(MovieSearchManagementService.SERVICE_NAME)
class MovieSearchManagementService extends ParallelExecutorService<List<MovieTitle>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieSearchManagementService.class);

    public static final String SERVICE_NAME = "movieSearchManagementService";

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Autowired
    private Set<SearchClient> searchClients;

    public MovieSearchManagementService() {
        super(DEFAULT_THREADS, DEFAULT_TIMEOUT_MILLIES);
    }

    public List<MovieTitle> search(final String queryString, final int page) throws SearchServiceException {
        try {
            List<Callable<List<MovieTitle>>> tasks = new ArrayList<>();
            for (final SearchClient searchClient : searchClients) {
                tasks.add(new Callable<List<MovieTitle>>() {
                    @Override
                    public List<MovieTitle> call() {
                        try {
                            return searchClient.search(queryString, page);
                        } catch (SearchServiceException e) {
                            LOGGER.error("An error occurred while executing {]", searchClient.getClass().getSimpleName(), e);
                        } catch (Exception e) {
                            LOGGER.error("An unexpected error  occurred while executing {]", searchClient.getClass().getSimpleName(), e);
                        }
                        return Collections.emptyList();
                    }
                });
            }

            List<List<MovieTitle>> results = execute(tasks);

            return merge(results);
        } catch (Exception e) {
            LOGGER.error("An unexpected error occurred while searching", e);
            throw new IllegalStateException(e);
        }
    }

    public List<MovieTitle> getNowPlaying() {
        try {
            List<Callable<List<MovieTitle>>> tasks = new ArrayList<>();
            for (final SearchClient searchClient : searchClients) {
                tasks.add(new Callable<List<MovieTitle>>() {
                    @Override
                    public List<MovieTitle> call() {
                        try {
                            return searchClient.getNowPlaying();
                        } catch (SearchServiceException e) {
                            LOGGER.error("An error occurred while executing {}", searchClient.getClass().getSimpleName(), e);
                        } catch (Exception e) {
                            LOGGER.error("An unexpected error  occurred while executing {}", searchClient.getClass().getSimpleName(), e);
                        }
                        return Collections.emptyList();
                    }
                });
            }

            List<List<MovieTitle>> results = execute(tasks);

            return merge(results);
        } catch (Exception e) {
            LOGGER.error("An unexpected error occurred while searching", e);
            throw new IllegalStateException(e);
        }
    }

    private List<MovieTitle> merge(final List<List<MovieTitle>> results) {
        Map<String, MovieTitle> merged = new HashMap<>();

        for (List<MovieTitle> clientResults : results) {
            for (MovieTitle movieTitle : clientResults) {
                MovieTitle mergedMovieTitle = merged.get(movieTitle.getTitle());
                if (mergedMovieTitle == null) {
                    mergedMovieTitle = movieTitle;
                } else {
                    mergedMovieTitle.getIds().putAll(movieTitle.getIds());
                }
                merged.put(movieTitle.getTitle(), mergedMovieTitle);
            }
        }
        LOGGER.debug("Merged list size is {}", merged.size());
        return new ArrayList<>(merged.values());
    }

    void setSearchClients(Set<SearchClient> searchClients) {
        this.searchClients = searchClients;
    }
}
