package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.web.client.RottentomatoesApiClient;
import com.movierama.web.client.rottentomatoes.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component("rottentomatoesSearchClient")
class RottentomatoesSearchClient implements SearchClient {

    public static final String ID = "rottentomattoes";

    @Autowired
    private RottentomatoesApiClient rottentomatoesApiClient;

    @Override
    public List<MovieTitle> search(String queryString, int page) throws SearchServiceException {
        try {
            RottentomatoesMovieList response = rottentomatoesApiClient.searchMovie(queryString, page);
            return parseResponse(response);
        } catch (ExecutionException | InterruptedException e) {
            throw new SearchServiceException("An error occurred while executing the call", e);
        }
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public List<MovieTitle> getNowPlaying() throws SearchServiceException {
        try {
            RottentomatoesMovieList response = rottentomatoesApiClient.getNowPlaying();
            return parseResponse(response);
        } catch (ExecutionException | InterruptedException e) {
            throw new SearchServiceException("An error occurred while executing the call", e);
        }
    }

    @Override
    public MovieResult getMovie(MovieTitle movieTitle) throws SearchServiceException {
        String movieId = movieTitle.getIds().get(getId());
        if (movieId == null) {return null;}

        RottentomatoesMovie movieInfo = rottentomatoesApiClient.getMovieInfo(movieId);
        try {
            // sleep for 200ms to avoid getting 403(Forbidden) from rottentomatoes
            Thread.sleep(200);
        } catch (InterruptedException e) {
        }
        RottentomatoesMovieReviews movieReviews = rottentomatoesApiClient.getMovieReviews(movieId);

        return parseResponse(movieInfo, movieReviews);
    }

    private MovieResult parseResponse(RottentomatoesMovie rottentomatoesMovie, RottentomatoesMovieReviews rottentomatoesMovieReviews) {
        MovieResult movieResult = new MovieResult();

        movieResult.setTitle(rottentomatoesMovie.getTitle());
        movieResult.setReleaseYear(rottentomatoesMovie.getYear());
        movieResult.setSynopsis(rottentomatoesMovie.getSynopsis());

        if (rottentomatoesMovie.getCast() != null) {
            movieResult.setCast(new ArrayList<String>());
            for (RottentomatoesMovieCast actor : rottentomatoesMovie.getCast()) {
                movieResult.getCast().add(actor.getName());
            }
        }

        movieResult.setReviews(rottentomatoesMovieReviews.getTotalReviews());

        return movieResult;
    }

    private List<MovieTitle> parseResponse(RottentomatoesMovieList response) throws ExecutionException, InterruptedException {
        if (response.getMovies() == null) {return Collections.emptyList();}

        List<MovieTitle> movieResults = new ArrayList<>();

        for (RottentomatoesMovieTitle rottentomatoesMovieTitle : response.getMovies()) {
            MovieTitle movieTitle = new MovieTitle();

            movieTitle.getIds().put(getId(), rottentomatoesMovieTitle.getId());
            movieTitle.setTitle(rottentomatoesMovieTitle.getTitle());

            movieResults.add(movieTitle);
        }

        return movieResults;
    }

    void setRottentomatoesApiClient(RottentomatoesApiClient rottentomatoesApiClient) {
        this.rottentomatoesApiClient = rottentomatoesApiClient;
    }
}
