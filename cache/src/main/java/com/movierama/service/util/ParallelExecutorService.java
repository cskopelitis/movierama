package com.movierama.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public abstract class ParallelExecutorService<T> {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public static final int DEFAULT_TIMEOUT_MILLIES = 3000;
    public static final int DEFAULT_THREADS = 3;

    private int maxThreads;
    public int timeout;

    protected ParallelExecutorService(int maxThreads, int timeout) {
        this.maxThreads = maxThreads;
        this.timeout = timeout;
    }

    protected List<T> execute(Collection<Callable<T>> tasks) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(maxThreads);

        List<T> opResults = new ArrayList<>();

        logger.debug("Invoking {} tasks on {} thread(s)", tasks.size(), maxThreads);
        long t0 = System.currentTimeMillis();

        List<Future<T>> results = executor.invokeAll(tasks, timeout, TimeUnit.MILLISECONDS);

        for (Future<T> result : results) {
            try {
                T opResult = result.get();
                opResults.add(opResult);
            } catch (CancellationException ce) {
                logger.warn("One of the tasks ({}) timed out", result, ce);
            }
        }

        // wait for the tasks to finish
        executor.shutdown();

        logger.debug("Completed {}/{} task(s) in {} ms", opResults.size(), tasks.size(), System.currentTimeMillis() - t0);

        return opResults;
    }
}
