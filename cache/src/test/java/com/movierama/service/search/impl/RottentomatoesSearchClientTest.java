package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.web.client.RottentomatoesApiClient;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovie;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieList;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieReviews;
import com.movierama.web.client.rottentomatoes.json.RottentomatoesMovieTitle;
import com.movierama.web.util.JsonObjectMapperUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class RottentomatoesSearchClientTest {

    private static final String JSON_MOVIE_1 = "{\"title\":\"10 Cloverfield Lane\",\"releaseYear\":\"2016\",\"cast\":[\"Mary Elizabeth Winstead\",\"John Goodman\",\"John Gallagher Jr.\",\"Bradley Cooper\",\"Cindy Hogan\"],\"synopsis\":\"A young woman wakes up after a terrible accident to find that she's locked in a cellar with a doomsday prepper, who insists that he saved her life and that the world outside is uninhabitable following an apocalyptic catastrophe. Uncertain what to believe, the woman soon determines that she must escape at any cost.\",\"reviews\":0,\"key\":\"10 Cloverfield Lane\"}";
    private static final String JSON_MOVIE_2 = "{\"title\":\"Batman v Superman: Dawn of Justice\",\"releaseYear\":\"2016\",\"cast\":[\"Ben Affleck\",\"Henry Cavill\",\"Amy Adams\",\"Diane Lane\",\"Laurence Fishburne\"],\"synopsis\":\"Fearing the actions of a god-like Super Hero left unchecked, Gotham City?s own formidable, forceful vigilante takes on Metropolis?s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it?s ever known before.\",\"reviews\":0,\"key\":\"Batman v Superman: Dawn of Justice\"}";

    private RottentomatoesApiClient rottentomatoesApiClientMock;

    private RottentomatoesMovieTitle rottentomatoesMovieTitle1;
    private RottentomatoesMovieTitle rottentomatoesMovieTitle2;

    private RottentomatoesMovie rottentomatoesMovie1;
    private RottentomatoesMovie rottentomatoesMovie2;
    private RottentomatoesMovieReviews rottentomatoesMovieReviews;

    private final ObjectMapper objectMapper = new JsonObjectMapperUtils();

    private MockObjectFactory mockObjectFactory = new MockObjectFactory();

    @Before
    public void setUp() throws Exception {
        rottentomatoesApiClientMock = EasyMock.createMock(RottentomatoesApiClient.class);

        rottentomatoesMovie1 = objectMapper.readValue(JSON_MOVIE_1, RottentomatoesMovie.class);
        rottentomatoesMovieReviews = new RottentomatoesMovieReviews();
        rottentomatoesMovieReviews.setTotalReviews(5);

        rottentomatoesMovieTitle1 = new RottentomatoesMovieTitle();
        rottentomatoesMovieTitle1.setId("1");
        rottentomatoesMovieTitle1.setTitle(rottentomatoesMovie1.getTitle());

        rottentomatoesMovie2 = objectMapper.readValue(JSON_MOVIE_2, RottentomatoesMovie.class);
        rottentomatoesMovieTitle2 = new RottentomatoesMovieTitle();
        rottentomatoesMovieTitle2.setId("2");
        rottentomatoesMovieTitle2.setTitle(rottentomatoesMovie2.getTitle());
    }

    @Test
    public void testSearch() throws SearchServiceException {
        RottentomatoesMovieList rottentomatoesNowPlayingResponseMock = new RottentomatoesMovieList();
        rottentomatoesNowPlayingResponseMock.setMovies(new ArrayList<RottentomatoesMovieTitle>() {{
            add(rottentomatoesMovieTitle1);
        }});

        EasyMock.expect(rottentomatoesApiClientMock.searchMovie("query", 1)).andReturn(rottentomatoesNowPlayingResponseMock);

        RottentomatoesSearchClient rottentomatoesSearchClient = new RottentomatoesSearchClient();
        rottentomatoesSearchClient.setRottentomatoesApiClient(rottentomatoesApiClientMock);

        EasyMock.replay(rottentomatoesApiClientMock);

        List<MovieTitle> result = rottentomatoesSearchClient.search("query", 1);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetNowPlaying() throws SearchServiceException {
        RottentomatoesMovieList rottentomatoesNowPlayingResponseMock = new RottentomatoesMovieList();
        rottentomatoesNowPlayingResponseMock.setMovies(new ArrayList<RottentomatoesMovieTitle>() {{
            add(rottentomatoesMovieTitle1);
            add(rottentomatoesMovieTitle2);
        }});

        EasyMock.expect(rottentomatoesApiClientMock.getNowPlaying()).andReturn(rottentomatoesNowPlayingResponseMock);

        RottentomatoesSearchClient rottentomatoesSearchClient = new RottentomatoesSearchClient();
        rottentomatoesSearchClient.setRottentomatoesApiClient(rottentomatoesApiClientMock);

        EasyMock.replay(rottentomatoesApiClientMock);

        List<MovieTitle> result = rottentomatoesSearchClient.getNowPlaying();

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void testGetMovie() throws SearchServiceException {
        RottentomatoesMovieList rottentomatoesNowPlayingResponseMock = new RottentomatoesMovieList();
        rottentomatoesNowPlayingResponseMock.setMovies(new ArrayList<RottentomatoesMovieTitle>() {{
            add(rottentomatoesMovieTitle1);
            add(rottentomatoesMovieTitle2);
        }});

        EasyMock.expect(rottentomatoesApiClientMock.getMovieInfo(rottentomatoesMovieTitle1.getId())).andReturn(rottentomatoesMovie1);
        EasyMock.expect(rottentomatoesApiClientMock.getMovieReviews(rottentomatoesMovieTitle1.getId())).andReturn(rottentomatoesMovieReviews);

        RottentomatoesSearchClient rottentomatoesSearchClient = new RottentomatoesSearchClient();
        rottentomatoesSearchClient.setRottentomatoesApiClient(rottentomatoesApiClientMock);

        EasyMock.replay(rottentomatoesApiClientMock);

        MovieTitle rtMovieTitle1 = mockObjectFactory.createMovieTitle(rottentomatoesMovieTitle1.getTitle(), new HashMap<String, String>() {{
            put(RottentomatoesSearchClient.ID, rottentomatoesMovieTitle1.getId());
        }});
        MovieResult movieResult = rottentomatoesSearchClient.getMovie(rtMovieTitle1);

        Assert.assertNotNull(movieResult);
        Assert.assertEquals(rottentomatoesMovie1.getTitle(), movieResult.getTitle());
        Assert.assertEquals(rottentomatoesMovieReviews.getTotalReviews(), movieResult.getReviews());
        Assert.assertEquals(rottentomatoesMovie1.getSynopsis(), movieResult.getSynopsis());
        Assert.assertEquals(rottentomatoesMovie1.getYear(), movieResult.getReleaseYear());
        //todo  Assert.assertEquals(3, movieResult.getCast().size());
        Assert.assertEquals(0, movieResult.getIds().size());
    }
}
