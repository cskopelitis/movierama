package com.movierama.service.search.impl;

import com.movierama.cache.NowPlayingCache;
import com.movierama.cache.SearchResultsCache;
import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * @author cskopelitis
 * @since 11/04/2016
 */
public class SearchServiceTest extends ParallelExecutorService<List<MovieResult>> {

    private static final String TITLE_1 = "title1";
    private static final String TITLE_1_RT_ID = "rt1";
    private static final String TITLE_1_MDB_ID = "mdb1";

    private static final String TITLE_2 = "title2";
    private static final String TITLE_2_RT_ID = "rt2";
    private static final String TITLE_2_MDB_ID = "mdb2";

    private static final String TITLE_3 = "title3";
    private static final String TITLE_3_RT_ID = "rt3";

    private SearchResultsCache searchResultsCacheMock;
    private NowPlayingCache nowPlayingCacheMock;
    private MovieDetailsSearchManagementService movieDetailsSearchManagementServiceMock;
    private MovieSearchManagementService movieSearchManagementServiceMock;

    private MovieTitle movieTitle1;
    private MovieResult movieResult1;
    private MovieTitle movieTitle2;
    private MovieResult movieResult2;

    private MockObjectFactory mockObjectFactory = new MockObjectFactory();

    public SearchServiceTest() {
        super(5, DEFAULT_TIMEOUT_MILLIES);
    }

    @Before
    public void setUp() throws Exception {
        searchResultsCacheMock = EasyMock.createMock(SearchResultsCache.class);
        nowPlayingCacheMock = EasyMock.createMock(NowPlayingCache.class);
        movieDetailsSearchManagementServiceMock = EasyMock.createMock(MovieDetailsSearchManagementService.class);
        movieSearchManagementServiceMock = EasyMock.createMock(MovieSearchManagementService.class);

        movieTitle1 = mockObjectFactory.createMovieTitle(TITLE_1, new HashMap<String, String>() {{
            put(RottentomatoesSearchClient.ID, TITLE_1_RT_ID);
            put(ThemoviedbSearchClient.ID, TITLE_1_MDB_ID);
        }});
        movieTitle2 = mockObjectFactory.createMovieTitle(TITLE_2, new HashMap<String, String>() {{
            put(ThemoviedbSearchClient.ID, TITLE_2_RT_ID);
            put(ThemoviedbSearchClient.ID, TITLE_2_MDB_ID);
        }});

        movieResult1 = mockObjectFactory.createMovieDetails(TITLE_1, "2015", new ArrayList<String>() {{
            add("cast1");
            add("cast2");
        }}, "synopsis1", 6);
        movieResult2 = mockObjectFactory.createMovieDetails(TITLE_2, null, new ArrayList<String>() {{
            add("cast3");
        }}, "synopsis2", 2);
    }

    @Test
    public void testSearch() throws Exception {
        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);
        nowPlayingTitles.add(movieTitle2);

        EasyMock.expect(searchResultsCacheMock.getSearchResults("query")).andReturn(new ArrayList<MovieTitle>());
        EasyMock.expect(movieSearchManagementServiceMock.search("query", 1)).andReturn(nowPlayingTitles);
        searchResultsCacheMock.cacheSearchResults("query", nowPlayingTitles);
        EasyMock.expectLastCall();
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle2)).andReturn(movieResult2);
        EasyMock.expectLastCall();

        SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setSearchResultsCache(searchResultsCacheMock);
        searchService.setMovieSearchManagementService(movieSearchManagementServiceMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(searchResultsCacheMock, movieSearchManagementServiceMock, movieDetailsSearchManagementServiceMock);

        List<MovieResult> results = searchService.search("query", 1, false);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());
    }

    @Test
    public void testSearchFromCache() throws Exception {

        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);

        EasyMock.expect(searchResultsCacheMock.getSearchResults("query")).andReturn(nowPlayingTitles);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1);
        EasyMock.expectLastCall();

        SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setSearchResultsCache(searchResultsCacheMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(searchResultsCacheMock, movieDetailsSearchManagementServiceMock);

        List<MovieResult> results = searchService.search("query", 1, false);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testSearchForceRefresh() throws Exception {
        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);
        nowPlayingTitles.add(movieTitle2);

        searchResultsCacheMock.flush("query");
        EasyMock.expectLastCall();
        EasyMock.expect(searchResultsCacheMock.getSearchResults("query")).andReturn(new ArrayList<MovieTitle>());
        EasyMock.expect(movieSearchManagementServiceMock.search("query", 1)).andReturn(nowPlayingTitles);
        searchResultsCacheMock.cacheSearchResults("query", nowPlayingTitles);
        EasyMock.expectLastCall();
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle2)).andReturn(movieResult2);
        EasyMock.expectLastCall();

        SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setSearchResultsCache(searchResultsCacheMock);
        searchService.setMovieSearchManagementService(movieSearchManagementServiceMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(searchResultsCacheMock, movieSearchManagementServiceMock, movieDetailsSearchManagementServiceMock);

        List<MovieResult> results = searchService.search("query", 1, true);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());
    }

    @Test
    public void testGetNowPlaying() throws Exception {
        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);

        EasyMock.expect(nowPlayingCacheMock.getNowPlaying()).andReturn(new ArrayList<MovieTitle>());
        EasyMock.expect(movieSearchManagementServiceMock.getNowPlaying()).andReturn(nowPlayingTitles);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1);
        nowPlayingCacheMock.cacheNowPlaying(nowPlayingTitles);
        EasyMock.expectLastCall();

        SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setNowPlayingCache(nowPlayingCacheMock);
        searchService.setMovieSearchManagementService(movieSearchManagementServiceMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(nowPlayingCacheMock, movieSearchManagementServiceMock, movieDetailsSearchManagementServiceMock);

        List<MovieResult> results = searchService.getNowPlaying();

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testGetNowPlayingFromCache() throws Exception {
        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);
        nowPlayingTitles.add(movieTitle2);

        EasyMock.expect(nowPlayingCacheMock.getNowPlaying()).andReturn(nowPlayingTitles);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle2)).andReturn(movieResult2);

        SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setNowPlayingCache(nowPlayingCacheMock);
        searchService.setMovieSearchManagementService(movieSearchManagementServiceMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(nowPlayingCacheMock, movieDetailsSearchManagementServiceMock);

        List<MovieResult> results = searchService.getNowPlaying();

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());
    }

    @Test
    public void testGetMovieMultipleClients() throws SearchServiceException, ExecutionException, InterruptedException {
        int threads = 10;

        List<MovieTitle> nowPlayingTitles = new ArrayList<>();
        nowPlayingTitles.add(movieTitle1);

        EasyMock.expect(searchResultsCacheMock.getSearchResults("query")).andReturn(new ArrayList<MovieTitle>()).times(threads);
        EasyMock.expect(movieSearchManagementServiceMock.search("query", 1)).andReturn(nowPlayingTitles).times(threads);
        searchResultsCacheMock.cacheSearchResults("query", nowPlayingTitles);
        EasyMock.expectLastCall().times(threads);
        EasyMock.expect(movieDetailsSearchManagementServiceMock.getMovie(movieTitle1)).andReturn(movieResult1).times(threads);
        EasyMock.expectLastCall();

        final SearchServiceImpl searchService = new SearchServiceImpl();
        searchService.setSearchResultsCache(searchResultsCacheMock);
        searchService.setMovieSearchManagementService(movieSearchManagementServiceMock);
        searchService.setMovieDetailsSearchManagementService(movieDetailsSearchManagementServiceMock);

        EasyMock.replay(searchResultsCacheMock, movieSearchManagementServiceMock, movieDetailsSearchManagementServiceMock);

        List<Callable<List<MovieResult>>> tasks = new ArrayList<>();
        for (int i = 0; i < threads; i++) {
            tasks.add(new Callable<List<MovieResult>>() {
                @Override
                public List<MovieResult> call() throws Exception {
                    return searchService.search("query", 1, false);
                }
            });
        }

        List<List<MovieResult>> results = execute(tasks);

        Assert.assertNotNull(results);
        Assert.assertEquals(threads, results.size());
        for (List<MovieResult> result : results) {
            for (MovieResult movieResult : result) {
                Assert.assertEquals(TITLE_1, movieResult.getTitle());
                Assert.assertEquals(6, movieResult.getReviews());
                Assert.assertEquals("synopsis1", movieResult.getSynopsis());
                Assert.assertEquals("2015", movieResult.getReleaseYear());
                Assert.assertEquals(2, movieResult.getCast().size());
                Assert.assertEquals(0, movieResult.getIds().size());
            }
        }
    }
}
