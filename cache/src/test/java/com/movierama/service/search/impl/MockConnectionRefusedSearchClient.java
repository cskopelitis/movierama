package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;

import java.net.ConnectException;
import java.util.List;

/**
 * @author cskopelitis
 * @since 08/04/2016
 */
public class MockConnectionRefusedSearchClient implements SearchClient {

    @Override
    public List<MovieTitle> search(String queryString, int page) throws SearchServiceException {
        throw new SearchServiceException("connection refused", new ConnectException());
    }

    @Override
    public MovieResult getMovie(MovieTitle movieTitle) throws SearchServiceException {
        throw new SearchServiceException("connection refused", new ConnectException());
    }

    @Override
    public String getId() {
        return getClass().getSimpleName();
    }

    @Override
    public List<MovieTitle> getNowPlaying() throws SearchServiceException {
        throw new SearchServiceException("connection refused", new ConnectException());
    }
}
