package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;

import java.util.Collections;
import java.util.List;

/**
 * @author cskopelitis
 * @since 08/04/2016
 */
public class MockStallingSearchClient implements SearchClient {

    private long stallTimeMillies;

    public MockStallingSearchClient(long stallTimeMillies) {
        this.stallTimeMillies = stallTimeMillies;
    }

    @Override
    public List<MovieTitle> search(String queryString, int page) throws SearchServiceException {
        try {
            Thread.sleep(stallTimeMillies);
        } catch (InterruptedException ignored) {
        }

        return Collections.emptyList();
    }

    @Override
    public MovieResult getMovie(MovieTitle movieTitle) throws SearchServiceException {
        try {
            Thread.sleep(stallTimeMillies);
        } catch (InterruptedException ignored) {
        }
        return null;
    }

    @Override
    public String getId() {
        return getClass().getSimpleName();
    }

    @Override
    public List<MovieTitle> getNowPlaying() throws SearchServiceException {
        try {
            Thread.sleep(stallTimeMillies);
        } catch (InterruptedException ignored) {
        }

        return Collections.emptyList();
    }
}
