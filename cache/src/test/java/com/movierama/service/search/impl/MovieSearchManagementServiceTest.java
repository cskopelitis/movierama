package com.movierama.service.search.impl;

import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class MovieSearchManagementServiceTest {

    private static final String TITLE_1 = "title1";
    private static final String TITLE_1_RT_ID = "rt1";
    private static final String TITLE_1_MDB_ID = "mdb1";

    private static final String TITLE_2 = "title2";
    private static final String TITLE_2_MDB_1 = "mdb2";

    private static final String TITLE_3 = "title3";
    private static final String TITLE_3_RT_ID = "rt3";

    private SearchClient searchClientMock1;
    private SearchClient searchClientMock2;
    private SearchClient searchClientMockStalling;
    private SearchClient searchClientMockConnectException;

    private MockObjectFactory mockObjectFactory = new MockObjectFactory();

    private MovieTitle rtMovieTitle1;
    private MovieTitle mdbMovieTitle1;
    private MovieTitle mdbMovieTitle2;
    private MovieTitle rtMovieTitle3;

    @Before
    public void setUp() throws Exception {
        searchClientMock1 = EasyMock.createMock(SearchClient.class);
        searchClientMock2 = EasyMock.createMock(SearchClient.class);
        searchClientMockStalling = new MockStallingSearchClient(ParallelExecutorService.DEFAULT_TIMEOUT_MILLIES * 2);
        searchClientMockConnectException = new MockConnectionRefusedSearchClient();

        rtMovieTitle1 = mockObjectFactory.createMovieTitle(TITLE_1, new HashMap<String, String>() {{
            put(RottentomatoesSearchClient.ID, TITLE_1_RT_ID);
        }});
        mdbMovieTitle1 = mockObjectFactory.createMovieTitle(TITLE_1, new HashMap<String, String>() {{
            put(ThemoviedbSearchClient.ID, TITLE_1_MDB_ID);
        }});
        mdbMovieTitle2 = mockObjectFactory.createMovieTitle(TITLE_2, new HashMap<String, String>() {{
            put(ThemoviedbSearchClient.ID, TITLE_2_MDB_1);
        }});
        rtMovieTitle3 = mockObjectFactory.createMovieTitle(TITLE_3, new HashMap<String, String>() {{
            put(RottentomatoesSearchClient.ID, TITLE_3_RT_ID);
        }});
    }

    @Test
    public void testSearchMergeList() throws SearchServiceException {
        List<MovieTitle> client1Result = Collections.singletonList(rtMovieTitle1);
        EasyMock.expect(searchClientMock1.search(TITLE_1, 1)).andReturn(client1Result).once();
        List<MovieTitle> client2Result = Collections.singletonList(mdbMovieTitle1);
        EasyMock.expect(searchClientMock2.search(TITLE_1, 1)).andReturn(client2Result).once();

        MovieSearchManagementService searchAggregator = new MovieSearchManagementService();
        searchAggregator.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMock2);
        }});

        EasyMock.replay(searchClientMock1, searchClientMock2);

        List<MovieTitle> movieResults = searchAggregator.search(TITLE_1, 1);

        Assert.assertNotNull(movieResults);
        Assert.assertEquals(1, movieResults.size());

        MovieTitle movieTitle = movieResults.iterator().next();
        Assert.assertEquals(TITLE_1, movieTitle.getTitle());
        Assert.assertEquals(2, movieTitle.getIds().size());
        Assert.assertEquals(TITLE_1_RT_ID, movieTitle.getIds().get(RottentomatoesSearchClient.ID));
        Assert.assertEquals(TITLE_1_MDB_ID, movieTitle.getIds().get(ThemoviedbSearchClient.ID));
    }

    @Test
    public void testGetNowPlayingMergeList() throws SearchServiceException {
        List<MovieTitle> client1Result = Arrays.asList(rtMovieTitle1, rtMovieTitle3);
        EasyMock.expect(searchClientMock1.getNowPlaying()).andReturn(client1Result).once();
        List<MovieTitle> client2Result = Arrays.asList(mdbMovieTitle1, mdbMovieTitle2);
        EasyMock.expect(searchClientMock2.getNowPlaying()).andReturn(client2Result).once();

        MovieSearchManagementService searchAggregator = new MovieSearchManagementService();
        searchAggregator.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMock2);
        }});

        EasyMock.replay(searchClientMock1, searchClientMock2);

        List<MovieTitle> movieResults = searchAggregator.getNowPlaying();

        Assert.assertNotNull(movieResults);
        Assert.assertEquals(3, movieResults.size());

        int titlesFound = 0;
        for (MovieTitle movieTitle : movieResults) {
            switch (movieTitle.getTitle()) {
                case TITLE_1:
                    Assert.assertEquals(2, movieTitle.getIds().size());
                    Assert.assertEquals(TITLE_1_RT_ID, movieTitle.getIds().get(RottentomatoesSearchClient.ID));
                    Assert.assertEquals(TITLE_1_MDB_ID, movieTitle.getIds().get(ThemoviedbSearchClient.ID));
                    titlesFound++;
                    break;
                case TITLE_2:
                    Assert.assertEquals(1, movieTitle.getIds().size());
                    Assert.assertEquals(TITLE_2_MDB_1, movieTitle.getIds().get(ThemoviedbSearchClient.ID));
                    titlesFound++;
                    break;
                case TITLE_3:
                    Assert.assertEquals(1, movieTitle.getIds().size());
                    Assert.assertEquals(TITLE_3_RT_ID, movieTitle.getIds().get(RottentomatoesSearchClient.ID));
                    titlesFound++;
                    break;
            }
        }
        Assert.assertEquals(3, titlesFound);
    }

    @Test
    public void testClientStalled() throws SearchServiceException {
        List<MovieTitle> client1Result = Arrays.asList(rtMovieTitle1, rtMovieTitle3);
        EasyMock.expect(searchClientMock1.getNowPlaying()).andReturn(client1Result).once();
        List<MovieTitle> client2Result = Arrays.asList(mdbMovieTitle1, mdbMovieTitle2);
        EasyMock.expect(searchClientMock2.getNowPlaying()).andReturn(client2Result).once();

        MovieSearchManagementService searchAggregator = new MovieSearchManagementService();
        searchAggregator.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMockStalling);
            add(searchClientMock2);
        }});

        EasyMock.replay(searchClientMock1, searchClientMock2);

        List<MovieTitle> movieResults = searchAggregator.getNowPlaying();

        Assert.assertNotNull(movieResults);
        Assert.assertEquals(3, movieResults.size());
    }

    @Test
    public void testClientConnectException() throws SearchServiceException {
        List<MovieTitle> client1Result = Arrays.asList(rtMovieTitle1, rtMovieTitle3);
        EasyMock.expect(searchClientMock1.getNowPlaying()).andReturn(client1Result).once();
        List<MovieTitle> client2Result = Arrays.asList(mdbMovieTitle1, mdbMovieTitle2);
        EasyMock.expect(searchClientMock2.getNowPlaying()).andReturn(client2Result).once();

        MovieSearchManagementService searchAggregator = new MovieSearchManagementService();
        searchAggregator.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMockConnectException);
            add(searchClientMock2);
        }});

        EasyMock.replay(searchClientMock1, searchClientMock2);

        List<MovieTitle> movieResults = searchAggregator.getNowPlaying();

        Assert.assertNotNull(movieResults);
        Assert.assertEquals(3, movieResults.size());
    }
}
