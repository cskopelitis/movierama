package com.movierama.service.search.impl;

import com.movierama.cache.MovieCache;
import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.SearchClient;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.service.util.ParallelExecutorService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class MovieDetailsSearchManagementServiceTest {

    private static final String TITLE = "title1";
    private static final String RELEASE_YEAR = "2015";
    private static final String SHORT_SYNOPSIS = "short description";
    private static final String LONG_SYNOPSIS = "very longer description";
    private static final String RT_ID = "rt1";
    private static final String MDB_ID = "mdb1";

    private MockObjectFactory mockObjectFactory = new MockObjectFactory();

    private MovieCache movieCacheMock;
    private SearchClient searchClientMock1;
    private SearchClient searchClientMock2;
    private SearchClient searchClientMockStalling;
    private SearchClient searchClientMockConnectException;

    private MovieTitle movieTitle;
    private MovieResult rtMovieResult;
    private MovieResult mdbMovieResult;
    private MovieResult movieResult;

    @Before
    public void setUp() throws Exception {
        movieCacheMock = EasyMock.createMock(MovieCache.class);
        searchClientMock1 = EasyMock.createMock(SearchClient.class);
        searchClientMock2 = EasyMock.createMock(SearchClient.class);
        searchClientMockStalling = new MockStallingSearchClient(ParallelExecutorService.DEFAULT_TIMEOUT_MILLIES * 2);
        searchClientMockConnectException = new MockConnectionRefusedSearchClient();

        movieTitle = mockObjectFactory.createMovieTitle(TITLE, new HashMap<String, String>() {{
            put(RottentomatoesSearchClient.ID, RT_ID);
            put(ThemoviedbSearchClient.ID, MDB_ID);
        }});
        rtMovieResult = mockObjectFactory.createMovieDetails(TITLE, RELEASE_YEAR, new ArrayList<String>() {{
            add("cast1");
            add("cast2");
        }}, SHORT_SYNOPSIS, 6);
        mdbMovieResult = mockObjectFactory.createMovieDetails(TITLE, null, new ArrayList<String>() {{
            add("cast1");
            add("cast3");
        }}, LONG_SYNOPSIS, 2);
        movieResult = mockObjectFactory.createMovieDetails(TITLE, RELEASE_YEAR, new ArrayList<String>() {{
            add("cast1");
            add("cast2");
            add("cast3");
        }}, LONG_SYNOPSIS, 8);
    }

    @Test
    public void testGetMovieMergeResults() throws SearchServiceException {

        EasyMock.expect(movieCacheMock.getMovie(movieTitle.getTitle())).andReturn(null).once();
        movieCacheMock.cacheMovie(movieResult);
        EasyMock.expectLastCall().once();
        EasyMock.expect(searchClientMock1.getMovie(movieTitle)).andReturn(rtMovieResult).once();
        EasyMock.expect(searchClientMock2.getMovie(movieTitle)).andReturn(mdbMovieResult).once();

        MovieDetailsSearchManagementService movieSearchManagementService = new MovieDetailsSearchManagementService();
        movieSearchManagementService.setMovieCache(movieCacheMock);
        movieSearchManagementService.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMock2);
        }});

        EasyMock.replay(movieCacheMock, searchClientMock1, searchClientMock2);

        MovieResult movieResult = movieSearchManagementService.getMovie(movieTitle);

        Assert.assertNotNull(movieResult);
        Assert.assertEquals(TITLE, movieResult.getTitle());
        Assert.assertEquals(8, movieResult.getReviews());
        Assert.assertEquals(LONG_SYNOPSIS, movieResult.getSynopsis());
        Assert.assertEquals(RELEASE_YEAR, movieResult.getReleaseYear());
        Assert.assertEquals(3, movieResult.getCast().size());
        Assert.assertEquals(0, movieResult.getIds().size());
    }

    @Test
    public void testGetMovieFromCache() throws SearchServiceException {
        MovieResult movieResult = mockObjectFactory.createMovieDetails(TITLE, RELEASE_YEAR, Arrays.asList("cast1", "cast2"), SHORT_SYNOPSIS, 5);

        EasyMock.expect(movieCacheMock.getMovie(movieTitle.getTitle())).andReturn(movieResult).once();

        MovieDetailsSearchManagementService movieSearchManagementService = new MovieDetailsSearchManagementService();
        movieSearchManagementService.setMovieCache(movieCacheMock);

        EasyMock.replay(movieCacheMock);

        MovieResult result = movieSearchManagementService.getMovie(movieTitle);

        Assert.assertNotNull(result);
        Assert.assertEquals(TITLE, result.getTitle());
        Assert.assertEquals(5, result.getReviews());
        Assert.assertEquals(SHORT_SYNOPSIS, result.getSynopsis());
        Assert.assertEquals(RELEASE_YEAR, result.getReleaseYear());
        Assert.assertEquals(2, result.getCast().size());
        Assert.assertEquals(0, result.getIds().size());
    }

    @Test
    public void testGetMovieClientStalled() throws SearchServiceException {
        EasyMock.expect(movieCacheMock.getMovie(movieTitle.getTitle())).andReturn(null).once();
        movieCacheMock.cacheMovie(movieResult);
        EasyMock.expectLastCall().once();
        EasyMock.expect(searchClientMock1.getMovie(movieTitle)).andReturn(rtMovieResult).once();
        EasyMock.expect(searchClientMock2.getMovie(movieTitle)).andReturn(mdbMovieResult).once();

        MovieDetailsSearchManagementService movieSearchManagementService = new MovieDetailsSearchManagementService();
        movieSearchManagementService.setMovieCache(movieCacheMock);
        movieSearchManagementService.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMock1);
            add(searchClientMock2);
            add(searchClientMockStalling);
        }});

        EasyMock.replay(movieCacheMock, searchClientMock1, searchClientMock2);

        MovieResult movieResult = movieSearchManagementService.getMovie(movieTitle);

        Assert.assertNotNull(movieResult);
        Assert.assertEquals(TITLE, movieResult.getTitle());
        Assert.assertEquals(8, movieResult.getReviews());
        Assert.assertEquals(LONG_SYNOPSIS, movieResult.getSynopsis());
        Assert.assertEquals(RELEASE_YEAR, movieResult.getReleaseYear());
        Assert.assertEquals(3, movieResult.getCast().size());
        Assert.assertEquals(0, movieResult.getIds().size());
    }

    @Test
    public void testGetMovieClientConnectException() throws SearchServiceException {

        EasyMock.expect(movieCacheMock.getMovie(movieTitle.getTitle())).andReturn(null).once();
        movieCacheMock.cacheMovie(movieResult);
        EasyMock.expectLastCall().once();
        EasyMock.expectLastCall().once();
        EasyMock.expect(searchClientMock1.getMovie(movieTitle)).andReturn(rtMovieResult).once();
        EasyMock.expect(searchClientMock2.getMovie(movieTitle)).andReturn(mdbMovieResult).once();

        MovieDetailsSearchManagementService movieSearchManagementService = new MovieDetailsSearchManagementService();
        movieSearchManagementService.setMovieCache(movieCacheMock);
        movieSearchManagementService.setSearchClients(new HashSet<SearchClient>() {{
            add(searchClientMockConnectException);
            add(searchClientMock1);
            add(searchClientMock2);
        }});

        EasyMock.replay(movieCacheMock, searchClientMock1, searchClientMock2);

        MovieResult movieResult = movieSearchManagementService.getMovie(movieTitle);

        Assert.assertNotNull(movieResult);
        Assert.assertEquals(TITLE, movieResult.getTitle());
        Assert.assertEquals(8, movieResult.getReviews());
        Assert.assertEquals(LONG_SYNOPSIS, movieResult.getSynopsis());
        Assert.assertEquals(RELEASE_YEAR, movieResult.getReleaseYear());
        Assert.assertEquals(3, movieResult.getCast().size());
        Assert.assertEquals(0, movieResult.getIds().size());
    }
}
