package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;
import com.movierama.service.search.exception.SearchServiceException;
import com.movierama.web.client.ThemoviedbApiClient;
import com.movierama.web.client.themoviedb.json.*;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author cskopelitis
 * @since 07/04/2016
 */
public class ThemoviedbSearchClientTest {

    private ThemoviedbApiClient themoviedbApiClientMock;
    private ThemoviedbMovieList themoviedbMovieList;

    private ThemoviedbMovieTitle themoviedbMovieTitle1;
    private ThemoviedbMovieTitle themoviedbMovieTitle2;

    private ThemoviedbMovie themoviedbMovie1;
    private ThemoviedbMovie themoviedbMovie2;

    private MockObjectFactory mockObjectFactory = new MockObjectFactory();

    @Before
    public void setUp() throws Exception {
        themoviedbApiClientMock = EasyMock.createMock(ThemoviedbApiClient.class);

        themoviedbMovie1 = new ThemoviedbMovie();
        themoviedbMovie1.setTitle("TITLE_1");
        themoviedbMovie1.setId("mdbId1");
        themoviedbMovie1.setOverview("overview");
        ThemoviedbMovieCredits themoviedbMovieCredits = new ThemoviedbMovieCredits();
        themoviedbMovieCredits.setCast(new ArrayList<ThemoviedbMovieCast>() {{
            ThemoviedbMovieCast themoviedbMovieCast1 = new ThemoviedbMovieCast();
            themoviedbMovieCast1.setName("cast1");
        }});
        themoviedbMovie1.setCredits(themoviedbMovieCredits);
        themoviedbMovie1.setReleaseDate("2014-6-4");
        ThemoviedbMovieReviews themoviedbMovieReviews = new ThemoviedbMovieReviews();
        themoviedbMovieReviews.setTotalReviews(5);
        themoviedbMovie1.setReviews(themoviedbMovieReviews);

        themoviedbMovieTitle1 = new ThemoviedbMovieTitle();
        themoviedbMovieTitle1.setId("mdbId1");
        themoviedbMovieTitle1.setTitle("TITLE_1");
        themoviedbMovieTitle2 = new ThemoviedbMovieTitle();
        themoviedbMovieTitle2.setId("mdbId2");
        themoviedbMovieTitle2.setTitle("TITLE_2");
    }

    @Test
    public void testSearch() throws SearchServiceException {
        ThemoviedbMovieList rottentomatoesNowPlayingResponseMock = new ThemoviedbMovieList();
        rottentomatoesNowPlayingResponseMock.setResults(new ArrayList<ThemoviedbMovieTitle>() {{
            add(themoviedbMovieTitle1);
        }});

        EasyMock.expect(themoviedbApiClientMock.searchMovie("query", 1)).andReturn(rottentomatoesNowPlayingResponseMock);

        ThemoviedbSearchClient themoviedbSearchClient = new ThemoviedbSearchClient();
        themoviedbSearchClient.setThemoviedbApiClient(themoviedbApiClientMock);

        EasyMock.replay(themoviedbApiClientMock);

        List<MovieTitle> result = themoviedbSearchClient.search("query", 1);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetNowPlaying() throws SearchServiceException {
        ThemoviedbMovieList rottentomatoesNowPlayingResponseMock = new ThemoviedbMovieList();
        rottentomatoesNowPlayingResponseMock.setResults(new ArrayList<ThemoviedbMovieTitle>() {{
            add(themoviedbMovieTitle1);
            add(themoviedbMovieTitle2);
        }});

        EasyMock.expect(themoviedbApiClientMock.getNowPlaying()).andReturn(rottentomatoesNowPlayingResponseMock);

        ThemoviedbSearchClient themoviedbSearchClient = new ThemoviedbSearchClient();
        themoviedbSearchClient.setThemoviedbApiClient(themoviedbApiClientMock);

        EasyMock.replay(themoviedbApiClientMock);

        List<MovieTitle> result = themoviedbSearchClient.getNowPlaying();

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void testGetMovie() throws SearchServiceException {
        ThemoviedbMovieList themoviedbMovieList = new ThemoviedbMovieList();
        themoviedbMovieList.setResults(new ArrayList<ThemoviedbMovieTitle>() {{
            add(themoviedbMovieTitle1);
        }});

        EasyMock.expect(themoviedbApiClientMock.getMovieInfo(themoviedbMovieTitle1.getId())).andReturn(themoviedbMovie1);

        ThemoviedbSearchClient themoviedbSearchClient = new ThemoviedbSearchClient();
        themoviedbSearchClient.setThemoviedbApiClient(themoviedbApiClientMock);

        EasyMock.replay(themoviedbApiClientMock);

        MovieTitle rtMovieTitle1 = mockObjectFactory.createMovieTitle(themoviedbMovieTitle1.getTitle(), new HashMap<String, String>() {{
            put(ThemoviedbSearchClient.ID, themoviedbMovieTitle1.getId());
        }});
        MovieResult movieResult = themoviedbSearchClient.getMovie(rtMovieTitle1);

        Assert.assertNotNull(movieResult);
        Assert.assertEquals(themoviedbMovie1.getTitle(), movieResult.getTitle());
        Assert.assertEquals(themoviedbMovie1.getReviews().getTotalReviews(), movieResult.getReviews());
        Assert.assertEquals(themoviedbMovie1.getOverview(), movieResult.getSynopsis());
        Assert.assertEquals("2014", movieResult.getReleaseYear());
        //todo  Assert.assertEquals(3, movieResult.getCast().size());
        Assert.assertEquals(0, movieResult.getIds().size());
    }
}
