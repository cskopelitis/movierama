package com.movierama.service.search.impl;

import com.movierama.dto.MovieResult;
import com.movierama.dto.MovieTitle;

import java.util.List;
import java.util.Map;

/**
 * @author cskopelitis
 * @since 10/04/2016
 */
public class MockObjectFactory {

    public MovieTitle createMovieTitle(String title, Map<String, String> ids) {
        MovieTitle movieTitle = new MovieTitle();
        movieTitle.setIds(ids);
        movieTitle.setTitle(title);
        return movieTitle;
    }

    public MovieResult createMovieDetails(String title, String releaseYear, List<String> cast, String synopsis, int reviews) {
        MovieResult movieResult = new MovieResult();
        movieResult.setTitle(title);
        movieResult.setCast(cast);
        movieResult.setReleaseYear(releaseYear);
        movieResult.setSynopsis(synopsis);
        movieResult.setReviews(reviews);
        return movieResult;
    }
}
